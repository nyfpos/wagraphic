﻿//#define LIB_TEST1
//#define LIB_TEST2
//#define LIB_TEST3
//#define LIB_TEST4
#define LIB_TEST5
//#define LIB_TEST6
//#define LIB_TEST6_1
//#define LIB_TEST6_2
//#define LIB_TEST7

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WA.GraphicLib;
using NGraphics;

namespace WAGraphicLibTest2
{
    public partial class Form1 : Form
    {
        private Stage _stage;

        private void _Loginfo(string format, params object[] args)
        {
            textBox2.Text = String.Format(format, args);
        }
        private void _LoginfoAppend(string format, params object[] args)
        {
            textBox2.Text += String.Format(format, args);
            textBox2.Text += "\r\n";
            textBox2.SelectionStart = textBox2.TextLength;
            textBox2.ScrollToCaret();
        }

        public Form1()
        {
            InitializeComponent();

            _stage = new Stage(graphicControl1);
            _stage.color = 0x333333;
            _stage.addEventListener(EventType.StageOnUpdate, (evt) => textBox1.Text = "(" + Stage.CursorPos.X + "," + Stage.CursorPos.Y + ")");
#if LIB_TEST1
            _stage.addEventListener(EventType.StageOnLoaded, _OnLoaded);
#endif
#if LIB_TEST2
            _stage.addEventListener(EventType.StageOnLoaded, _OnLoaded2);
#endif
#if LIB_TEST3
            _stage.addEventListener(EventType.StageOnLoaded, _OnLoaded3);
#endif
#if LIB_TEST4
            _stage.addEventListener(EventType.StageOnLoaded, _OnLoaded4);
#endif
#if LIB_TEST5
            _stage.addEventListener(EventType.StageOnLoaded, _OnLoaded5);
#endif 
#if LIB_TEST6
            _stage.addEventListener(EventType.StageOnLoaded, _OnLoaded6);
#endif
#if LIB_TEST7
            _stage.addEventListener(EventType.StageOnLoaded, _OnLoaded7);
#endif
        }
#if LIB_TEST7
        private void _OnLoaded7(Event evt)
        {
            Logger.Instance.BindLogInfo(_LoginfoAppend);
            Image img = Image.FromFile(@"../../../Resource/ADAM4000-S.png");
            var waimg = new DrawImage();
            waimg.draw(img);
            waimg.color = 0xFFFFFF;
            waimg.interactive.bedrag = true;
            _stage.add(waimg);
            //FF7F00
            waimg.addEventListener(EventType.MouseDown, (e) => waimg.color = 0xFF7F00);
            waimg.addEventListener(EventType.MouseUp, (e) => waimg.color = 0xFFFFFF);
        }
#endif
#if LIB_TEST6
        private void _OnLoaded6(Event evt)
        {
            Logger.Instance.BindLogInfo(_LoginfoAppend);

            uint[] colors = new uint[]
            {
                0xF4A460,
                0x8B4513,
                0x20B2AA,
                0x7FFFD4
            };

            Sprite[] ss = new Sprite[4];
            Quad[] qs = new Quad[4];
            for (int i = 0; i < qs.Length; i++)
            {
                qs[i] = new Quad();
                ss[i] = new Sprite();
            }
            Rect[] drawinfos = new Rect[]
            {
                new Rect(10,10,50,30),
                new Rect(30,30,50,30),
                new Rect(50,50,50,30),
                new Rect(70,70,50,30),
            };

            for (int i = 0; i < drawinfos.Length; i++)
            {
                qs[i].x = (float)drawinfos[i].X;
                qs[i].y = (float)drawinfos[i].Y;
                qs[i].draw(0, 0, (float)drawinfos[i].Width, (float)drawinfos[i].Height);

                qs[i].beFillColor = true;
                qs[i].color =
                qs[i].fillColor = colors[i];
                qs[i].fillAlpha = 0.5f;
#if LIB_TEST6_1
                _stage.add(qs[i]);
                int j = i;                
                qs[i].addEventListener(EventType.MouseDown, (e) => _LoginfoAppend("obj{0}-MouseDown", j));
                qs[i].addEventListener(EventType.MouseUp, (e) => _LoginfoAppend("obj{0}-MouseUp", j));
                qs[i].addEventListener(EventType.MouseClick, (e) => _LoginfoAppend("obj{0}-MouseClick", j));
                qs[i].addEventListener(EventType.MouseEnter, (e) => _LoginfoAppend("obj{0}-MouseEnter", j));
                qs[i].addEventListener(EventType.MouseLeave, (e) => _LoginfoAppend("obj{0}-MouseLeave", j));
                qs[i].interactive.bedrag = true;
#endif
            }
#if LIB_TEST6_2
            ss[0].addChild(qs[0]);
            ss[0].addChild(qs[1]);
            ss[1].addChild(qs[2]);
            ss[1].addChild(qs[3]);

            ss[0].showBoundary = true;
            ss[1].showBoundary = true;
            _stage.add(ss[0]);
            _stage.add(ss[1]);

            const int COUNT_SPRITE = 2;
            for (int i = 0; i < COUNT_SPRITE; i++)
            {
                int j = i;
                ss[i].interactive.bedrag = true;
                ss[i].addEventListener(EventType.MouseDown, (e) => _LoginfoAppend("sprite{0}-MouseDown", j));
                ss[i].addEventListener(EventType.MouseUp, (e) => _LoginfoAppend("sprite{0}-MouseUp", j));
                ss[i].addEventListener(EventType.MouseClick, (e) => _LoginfoAppend("sprite{0}-MouseClick", j));
                ss[i].addEventListener(EventType.MouseEnter, (e) => _LoginfoAppend("sprite{0}-MouseEnter", j));
                ss[i].addEventListener(EventType.MouseLeave, (e) => _LoginfoAppend("sprite{0}-MouseLeave", j));
            }
#endif
        }
#endif
#if LIB_TEST5
        private void _OnLoaded5(Event evt)
        {
            Logger.Instance.BindLogInfo(_LoginfoAppend);

            uint[] colors = new uint[]
            {
                0xF4A460,
                0x8B4513,
                0x20B2AA,
                0x7FFFD4
            };

            Quad[] qs = new Quad[4];
            for (int i = 0; i < qs.Length; i++)
                qs[i] = new Quad();

            Rect[] drawinfos = new Rect[]
            {
                new Rect(10,10,50,30),
                new Rect(30,30,50,30),
                new Rect(50,50,50,30),
                new Rect(70,70,50,30),
            };

            for (int i = 0; i < drawinfos.Length; i++)
            {
                qs[i].x = (float)drawinfos[i].X;
                qs[i].y = (float)drawinfos[i].Y;
                qs[i].draw(0, 0, (float)drawinfos[i].Width, (float)drawinfos[i].Height);

                qs[i].beFillColor = true;
                qs[i].color =
                qs[i].fillColor = colors[i];
                qs[i].fillAlpha = 0.5f;
                _stage.add(qs[i]);

                int j = i;
                qs[i].addEventListener(EventType.MouseDown, (e) => _LoginfoAppend("obj{0}-MouseDown", j));
                qs[i].addEventListener(EventType.MouseUp, (e) => _LoginfoAppend("obj{0}-MouseUp", j));
                qs[i].addEventListener(EventType.MouseClick, (e) => _LoginfoAppend("obj{0}-MouseClick", j));
                qs[i].addEventListener(EventType.MouseEnter, (e) => _LoginfoAppend("obj{0}-MouseEnter", j));
                qs[i].addEventListener(EventType.MouseLeave, (e) => _LoginfoAppend("obj{0}-MouseLeave", j));
            }
        }
#endif

#if LIB_TEST4
        private void _OnLoaded4(Event evt)
        {
            NGraphics.Point st = new NGraphics.Point(10,10), 
                            ed=new NGraphics.Point(100,100);
            
            var path = new DrawPath();
            path.border = 1.51f;
            path.color = 0xFFFFFF;
            _stage.add(path);

            bool end = false;
            float during = 0.0f;
            float t = 0.0f;
            _stage.addEventListener(EventType.StageOnUpdate, (e) =>
            {
                float deltaTime = (float)e.data;
                deltaTime /= 1000.0f;
                during += deltaTime;
                if (during > 2 && !end)
                {
                    if (t >= 1)
                    {
                        t = 1;
                        end = true;
                    }

                    var p = Util.lerp_pt(st, ed, t);
                    path.draw(new List<PathOp>() {
                                    new MoveTo(st.X,st.Y),
                                    new LineTo(p.X,p.Y)
                                });

                    t += deltaTime;
                }
            });
        }
#endif

#if LIB_TEST3
        private void _OnLoaded3(Event evt)
        {
            NGraphics.Point p0 = new NGraphics.Point(100, 100);
            NGraphics.Point p1 = new NGraphics.Point(180, 100);

            NGraphics.Point cp0 = new NGraphics.Point(120, 50);
            NGraphics.Point cp1 = new NGraphics.Point(160, 150);

            var ops = new List<PathOp>()
            {
                new MoveTo(p0.X,p0.Y)
            };

            var path = new DrawPath();
            path.border = 1.51f;
            path.color = 0xFFFFFF;

            _stage.add(path);
           /* 
            for (int i = 0; i <= 11; i++)
            {
                float t = (float)i / 11.0f; ;
                var p = Util.ApproximateBezier(p0, cp0, cp1, p1, t);
                ops.Add(new LineTo(p));
                path.draw(ops);
            }
            _Loginfo("Count:{0}", ops.Count);
             */
            
            bool end = false;
            float during = 0.0f;
            float t = 0.0f;
            _stage.addEventListener(EventType.StageOnUpdate, (e) =>
            {
                float deltaTime = (float)e.data;
                deltaTime /= 1000.0f;
                during += deltaTime;
                if (during > 3 && !end)
                {                    
                    if (t >= 1)
                    {
                        t = 1;
                        end = true;
                    }                                           
                    var p = Util.ApproximateBezier(p0, cp0, cp1, p1, t);
                    ops.Add(new LineTo(p));
                    path.draw(ops);
                    _LoginfoAppend("t:{0} p:{1},{2}\r\n", t, p.X, p.Y);
                    t += deltaTime;
                }
            });
            
        }
#endif
#if LIB_TEST2
        private void _OnLoaded2(Event evt)
        {
            var path = new DrawPath();
            path.border = 1.51f;

            path.color = 0xFFFFFF;
            path.fillColor = 0xFF0000;
            path.beFillColor = true;
            /*
            path.draw(new List<PathOp>()
            {
                new MoveTo(20,20),
                new LineTo(60,20),
                new LineTo(60,60),
                new LineTo(20,60),
                new LineTo(20,20)
            });
            */
            
            path.draw(new List<PathOp>()
            {
                new MoveTo(20,20),
                new CurveTo( new NGraphics.Point(40,-10), new NGraphics.Point(60,50), new NGraphics.Point(80,20))
            });
            
            path.scaleX = 1.5f;
            path.scaleY = 1.5f;            

            var s = new Sprite();
            s.addChild(path);
            s.showFrame = true;
            s.showBoundary = true;
//          s.rotateZ = 45;

            s.x = 100;
            s.y = 100;

            _Loginfo("{0},{1},{2},{3}", path.globalBoundary.p0.X, path.globalBoundary.p0.Y, path.globalBoundary.p3.X, path.globalBoundary.p3.Y);
            _stage.add(s);
        }
#endif

#if LIB_TEST1
        private void _OnLoaded(Event evt)
        {
            WA.GraphicLib.Ellipse el = new WA.GraphicLib.Ellipse();
            el.draw(0, 0, 20, 20);
            el.x = el.y = 90;
            el.color = 0x00FF00;
            el.border = 1.5f;
            _stage.add(el);

            Quad rect = new Quad();
            rect.draw(0, 0, 100, 50);
            rect.beFillColor = true;
            rect.fillColor =
            rect.color = 0xF4A460;
            rect.alpha = rect.fillAlpha = 0.8f;

            Quad rect2 = new Quad();
            rect2.draw(0, 0, 100, 50);
            rect2.beFillColor = true;
            rect2.fillColor =
            rect2.color = 0x8B4513;
            rect2.alpha = rect.fillAlpha = 0.8f;

            Sprite sprite = new Sprite();
            sprite.addChild(rect);
            sprite.pivot = new NGraphics.Point(50, 25);            

            sprite.x = 50;
            sprite.y = 50;

            Sprite sprite2 = new Sprite();
            sprite2.addChild(sprite);
            sprite2.addChild(rect2);
            rect2.x = 0;
            rect2.y = 0;
            sprite2.showFrame = true;
            sprite2.showBoundary = true;
            _stage.add(sprite2);

            sprite2.pivot = new NGraphics.Point(50, 25);
            sprite2.x = sprite2.y = 100;
            float during = 0.0f;
            _stage.addEventListener(EventType.StageOnUpdate, (e) => 
            {
                float deltaTime = (float)e.data;
                deltaTime /= 1000.0f;

                during += deltaTime;

                float factor =  20;
                factor *= deltaTime;
                
                sprite.rotateZ += factor;
                sprite2.rotateZ += factor;
                /*
                if (sprite.scaleX < 2)
                {
                    sprite.scaleX += factor;
                    sprite.scaleY += factor;
                }               
                if (sprite2.scaleX < 2 && during > 10)
                {
                    sprite2.scaleX += factor;
                    sprite2.scaleY += factor;
                }
                 */

                //_Loginfo("{0},{1}", sprite.x, sprite.y);
            });

        }
#endif
    }
}
