﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WA.GraphicLib;

namespace WA.Node
{
    public class DrawPathEx : DrawPath
    {
        private readonly float MinRangeX = 2.0f;
        private readonly float MinRangeY = 10.0f;
        private const float MinRange = 6;   

        public DrawPathEx() : base()
        {

        }
        
        public override void draw(IList<NGraphics.PathOp> operations)
        {
            base.draw(operations);
            if (_beUpdateBoundary)
            {
                _drawBoundary.left -= MinRangeX;
                _drawBoundary.right += MinRangeX;
                _drawBoundary.top -= MinRangeY;
                _drawBoundary.bottom += MinRangeY;
            }
        }
        
        public override bool hitTestPoint(float px, float py)
        {
            Boundary boundary = globalBoundary;
            float centerx = boundary.left + boundary.dw * 0.5f;
            float centery = boundary.top + boundary.dh * 0.5f;
            float r = Math.Max(boundary.dw, boundary.dh) * 0.5f + MinRange;
            float dx = px - centerx;
            float dy = py - centery;
            float dist = dx * dx + dy * dy;
            if (dist <= r*r)
            {
                return true;
            }
            return base.hitTestPoint(px, py);
        }
    }
}
