﻿using NGraphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WA.GraphicLib;

namespace WA.Node
{
    public class Wire :EventDispatcher
    {
        private const float MinRange = 6;        

        public Node node1
        {
            get;
            private set;
        }
        public Node node2
        {
            get;
            private set;
        }
        static public float lastWireZ = -100;

        private DrawPathEx _link;
        private bool _befocus;

        public Wire(Node n1, Node n2)
        {
            node1 = n1;
            node2 = n2;
            _befocus = false;
            _link = new DrawPathEx();            
            _link.z = lastWireZ;
            _link.border = 3.0f;
            _link.color = 0x808080;         
            redraw();
        }

        public DisplayObject disp
        {
            get
            {
                return _link;
            }
        }

        public bool befocus
        {
            get
            {
                return _befocus;
            }
            set
            {
                _befocus = value;
                if (_befocus)
                {
                    --_link.z;
                    _link.color = 0xFF7F00;
                }
                else
                {
                    _link.z = lastWireZ;
                    _link.color = 0x808080;
                }
            }
        }

#if ShowCurveTestRange  
        private Sprite _testsp;
#endif

        public bool hitTestPoint(NGraphics.Point pt)
        {
            bool ret = false;
            Point min, max;
            List<Boundary> listRect = new List<Boundary>();
            NGraphics.Point prev = new Point(double.MinValue, double.MinValue);
            foreach (NGraphics.Point orgpt in _link.eachBoundaryPoint())
            {
                if (prev.X != double.MinValue ||
                    prev.Y != double.MinValue)
                {
                    Util.GetMinMaxPoint(new Point[] { prev, orgpt }, out min, out max);
                    Boundary b = new Boundary();
                    b.left = (float)min.X;
                    b.top = (float)min.Y;
                    b.right = (float)max.X;
                    b.bottom = (float)max.Y;
                    listRect.Add(b);
                }

                prev = orgpt;
            }

#if ShowCurveTestRange 
            if (_testsp == null)
                _testsp = new Sprite();
            else
                _testsp.clear();
            Logger.Instance.ClearLogInfo();
            Logger.Instance.LogInfo("pt:{0},{1},{2}", pt.X, pt.Y, listRect.Count);
            for (int i = 0; i < listRect.Count; i++)
            {
                Boundary boundary = listRect[i];

                float centerx = boundary.left + boundary.dw * 0.5f;
                float centery = boundary.top + boundary.dh * 0.5f;
                float r = Math.Max(boundary.dw, boundary.dh)*0.5f + MinRange;

                // for test                
                var q = new WA.GraphicLib.Ellipse();
                q.z = -99999;
                q.draw(centerx-r, centery-r, 2*r,2*r);
                _testsp.addChild(q);
                var l = new WA.GraphicLib.Line();
                l.draw(new Point(centerx, centery), new Point(pt.X,pt.Y));
                l.z = -999999;
                _testsp.addChild(l);
                _testsp.z = -999999;
                Form1.getStage.add(_testsp);
                Logger.Instance.LogInfo("{0},{1},{2},{3}", centerx - r, centery - r, centerx - r + 2 * r, centery - r+2 * r);
            }
#endif            
            for (int i = 0; i < listRect.Count; i++ )
            {
                Boundary boundary = listRect[i];

                // exchange rect hit test for circle hit test...
                /*
                if(!(pt.X > boundary.right ||
                     pt.X < boundary.left ||
                     pt.Y > boundary.bottom ||
                     pt.Y < boundary.top))
                {
                    ret = true;
                    break;
                }
                 */
                float centerx = boundary.left + boundary.dw * 0.5f;
                float centery = boundary.top + boundary.dh * 0.5f;
                float r = Math.Max(boundary.dw, boundary.dh)*0.5f + MinRange;
                float dx = (float)pt.X - centerx;
                float dy = (float)pt.Y - centery;
                double dist = dx * dx + dy * dy;
                if (dist <= r*r)
                {
                    ret = true;
                    break;
                }
                else
                {

                }
            }
            return ret;
        }

        public void redraw()
        {
            NGraphics.Point st_pt = node1.glueOutputPos;
            NGraphics.Point ed_pt = node2.glueInputPos;

            float nw = (int)node1.disp.width,
                  nh = 3;

            Wire.drawLink(_link, st_pt, ed_pt, nw, nh);
        }

        static public void drawLink(DrawPath path, NGraphics.Point st_pt,
                                    NGraphics.Point ed_pt, float nw, float nh,
                                    int dir = 1)
        {
            float dx = (float)(ed_pt.X - st_pt.X);
            float dy = (float)(ed_pt.Y - st_pt.Y);
            float delta = (float)Math.Sqrt(dy * dy + dx * dx);
            float scale = 1;
            if (delta < nw)
            {
                scale = 0.75f - 0.75f * ((nw - delta) / nw);
            }
            path.draw(new List<PathOp>()
                    {
                        new MoveTo(st_pt),
                        new CurveTo(new NGraphics.Point(st_pt.X+dir*scale*nw, st_pt.Y+dir*nh),
                                    new NGraphics.Point(ed_pt.X-dir*scale*nw, ed_pt.Y-dir*nh),
                                    ed_pt)
                    });            
        }
    }

    public class Wires : EventDispatcher
    {
        private List<Wire> _tmpwires;
        private List<Wire> _wires;
        private Dictionary<string, Wire> _existWire;
        private Wire _curFocusWire;

        public Wires()
        {
            _wires = new List<Wire>();
            _tmpwires = new List<Wire>();
            _existWire = new Dictionary<string, Wire>();
            _curFocusWire = null;
        }

        public int Count
        {
            get
            {
                return _wires.Count;
            }
        }

        public IEnumerable each()
        {
            for (int i = 0; i < _wires.Count; i++)
            {
                yield return _wires[i];
            }
        }

        public Wire[] find(Node node)
        {
            _tmpwires.Clear();
            foreach (Wire wire in each())
            {
                if (wire.node1.guid == node.guid ||
                    wire.node2.guid == node.guid)
                {
                    _tmpwires.Add(wire);
                }
            }
            return _tmpwires.ToArray();
        }

        public Wire link(Node node1, Node node2)
        {
            if (node1 == null || node2 == null)
                return null;

            string key = node1.guid + node2.guid;

            if (!_existWire.ContainsKey(key))
            {
                Wire wire = new Wire(node1, node2);
                wire.disp.data = wire;
                wire.disp.addEventListener(EventType.MouseDown, _OnMouseDownWire, wire);
                _wires.Add(wire);
                _existWire.Add(key, wire);
                return wire;
            }
            return null;
        }

        public void remove(Wire wire)
        {
            string key = wire.node1.guid + wire.node2.guid;
            if (_wires.Remove(wire))
            {
                wire.disp.removeEventListener(EventType.MouseDown, _OnMouseDownWire);
            }
            _existWire.Remove(key);
        }

        private void _OnMouseDownWire(Event evt)
        {
            MouseEventArgs evtargs = (MouseEventArgs)evt.data;
            Point pos = new Point(evtargs.X, evtargs.Y);


            DisplayObject[] objs = evt.allTriggers as DisplayObject[];
            if (null != objs)
            {
                for (int i = 0; i < objs.Length; i++)
                {
                    if ( objs[i] is DrawPathEx &&
                         objs[i].data is Wire)
                    {
                        _HandleWireFocus((Wire)objs[i].data, pos);
                    }
                }
            }
        }

        private void _HandleWireFocus(Wire wire, Point pos)
        {
            if (wire != null && wire.hitTestPoint(pos))
            {
                if (_curFocusWire != null)
                    _curFocusWire.befocus = false;

                EventNodeArg arg = new EventNodeArg();
                arg.wire = wire;
                emit(NodeEventType.OnNodeWireClick, arg);
                wire.befocus = true;
                _curFocusWire = wire;
            }
        }

        public void clearFocusWire()
        {
            if (null != _curFocusWire)
            {
                _curFocusWire.befocus = false;
                _curFocusWire = null;
            }
        }

        public bool hasFocusAndClearFocus(NGraphics.Point pt)
        {
            int remain = _wires.Count;
            for (int i = 0; i < _wires.Count; i++)
            {
                if (_wires[i].befocus )
                {
                    if (!_wires[i].disp.hitTestPoint(pt) ||
                        !_wires[i].hitTestPoint(pt))
                    {
                        _wires[i].befocus = false;
                    }
                    else
                    {
                        continue;
                    }
                }
                --remain;
            }
            if (remain <= 0)
                _curFocusWire = null;

            return remain > 0 ;
        }
        
    };
}
