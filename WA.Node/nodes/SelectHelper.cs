﻿using NGraphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WA.GraphicLib;

namespace WA.Node
{
    internal enum InteractiveType
    {
        StageMouseDown=0,
        WireMouseDown,
        NodeMouseDown,
        NodeGlueMouseDown,
        StageMouseUp,
    }

    internal class InteractiveRequest
    {
        public InteractiveType type;
        public Event callbackEvent;
        public InteractiveRequest(InteractiveType t, Event e)
        {
            type = t;
            callbackEvent = e;
        }
    }

    internal enum SelectActions
    {
        None = 0,
        SelectingNodes,        
        AlreadySelectedNodes,
        MovingNodes,
        EndMoveNodes
    }

    internal class SelectHelper :EventDispatcher
    {
        private Stage _stage;
        private Quad _selectFrame;
        private Point _beginPt;
        private List<Node> _selNodes;
        private Func<IEnumerable<Node>> _eachNodes;
        private SelectActions _action;
        private List<InteractiveRequest> _interactiveReq;
        private InteractiveRequest _currentReq;
        private Wires _wires;
        private List<Point> _offsetPts;       

        public SelectHelper(Stage stage, Wires wires, Func<IEnumerable<Node>> eachNodes)
        {
            _stage = stage;
            _eachNodes = eachNodes;            
            _selectFrame = new Quad();
            _selectFrame.visible = false;
            _selectFrame.z = 999;

            _selectFrame.beFillColor = true;
            _selectFrame.fillColor = 0xA6BBCF;
            _selectFrame.fillAlpha = 0.5f;
            _selectFrame.color = 0xFF7F0E;
            _selectFrame.border = 2.0f;
            _selectFrame.dashPattern = new float[] { 4, 2 };

            _beginPt = Point.Zero;
            _selNodes = new List<Node>();
            _stage.add(_selectFrame);

            _action = SelectActions.None;

            _interactiveReq = new List<InteractiveRequest>();
            _currentReq = null;

            _offsetPts = new List<Point>();            
            _wires = wires;
        }

        public Node[] getFocusNodes()
        {
            return _selNodes.ToArray();
        }

        public bool bedrag
        { 
            get; set;        
        }

        public void OnWireMouseDown(Event e)
        {
            _interactiveReq.Add(new InteractiveRequest(InteractiveType.WireMouseDown, e));
        }

        public void OnNodeMouseDown(Event e)
        {
            _interactiveReq.Add(new InteractiveRequest(InteractiveType.NodeMouseDown, e));
        }

        public void OnNodeGlueMouseDown(Event e)
        {
            _interactiveReq.Add(new InteractiveRequest(InteractiveType.NodeGlueMouseDown, e));
        }

        private void _OnMouseDown(Event e)
        {
            _interactiveReq.Add(new InteractiveRequest(InteractiveType.StageMouseDown, e));
        }

        private void _OnMouseUp(Event e)
        {
            _interactiveReq.Add(new InteractiveRequest(InteractiveType.StageMouseUp, e));
        }

        public void start()
        {
            _addEventListeners();
        }

        public void stop()
        {
            _removeEventListeners();
        }

        private void _addEventListeners()
        {
            _stage.addEventListener(EventType.StageOnMouseDown, _OnMouseDown);
            _stage.addEventListener(EventType.StageOnMouseUp, _OnMouseUp);
            _stage.addEventListener(EventType.StageOnLastUpdate, _OnUpdate);
        }

        private void _removeEventListeners()
        {
            _stage.removeEventListener(EventType.StageOnMouseDown, _OnMouseDown);
            _stage.removeEventListener(EventType.StageOnMouseUp, _OnMouseUp);
            _stage.removeEventListener(EventType.StageOnLastUpdate, _OnUpdate);
        }

        private void _OnUpdate(Event evt)
        {            
            if (_selectAction())
            {
                InteractiveType type = _currentReq.type;
                Event e = _currentReq.callbackEvent;
                switch (type)
                {
                    case InteractiveType.NodeMouseDown:
                        _OnNodeMouseDownHandle(type, e);
                        break;
                    case InteractiveType.StageMouseDown:
                        _OnStageMouseDownHandle(type, e);
                        break;
                    case InteractiveType.StageMouseUp:
                        _OnStageMouseUpHandle(type, e);
                        break;
                    default:
                    case InteractiveType.NodeGlueMouseDown:
                    case InteractiveType.WireMouseDown:
                        _clearAction();                        
                        break;
                }
            }
            _interactiveReq.Clear();
            
            switch (_action)
            {
                case SelectActions.SelectingNodes:
                    _SelectingHandle();
                    break;
                case SelectActions.MovingNodes:
                    _MovingNodesHandle();
                    break;
            }            
        }

        private void _OnNodeMouseDownHandle(InteractiveType type, Event e)
        {
            if (!bedrag)
            {
                _clearAction();
                return;
            }

            EventNodeArg arg = (EventNodeArg)e.data;
            Node thisNd = arg.node;

            if (_selNodes.Count == 0 ||
                _action != SelectActions.AlreadySelectedNodes)
            {
                if (null != thisNd)
                {
                    _selNodes.Clear();
                    _selNodes.Add(thisNd);
                }
            }
            if (_selNodes.Count > 0)
            {
                float maxZ = float.MinValue;
                bool beFocusAtLeastOneNode = false;
                Point curpos = Stage.CursorPos;      
                bool beValidThisNd = thisNd != null && thisNd.disp.hitTestPoint(curpos);
                bool thisNdInSelAry = false;
                _offsetPts.Clear();
                for (int i = 0; i < _selNodes.Count; i++)
                {
                    if (thisNd.guid == _selNodes[i].guid)
                    {
                        thisNdInSelAry = true;
                    }
                    _offsetPts.Add(new Point(
                        curpos.X-_selNodes[i].disp.x,
                        curpos.Y-_selNodes[i].disp.y
                    ));
                    _selNodes[i].beFocus = true;
                    if (_selNodes[i].disp.hitTestPoint(curpos))
                    {
                        if (_selNodes[i].disp.z > maxZ)
                            maxZ = _selNodes[i].disp.z;
                        beFocusAtLeastOneNode = true;
                    }
                }
                if (beFocusAtLeastOneNode && beValidThisNd && 
                    (thisNdInSelAry || maxZ > thisNd.disp.z))
                    _action = SelectActions.MovingNodes;
                else
                {
                    _clearAction();
                    if (beValidThisNd)
                    {
                        _selNodes.Add(thisNd);
                        _offsetPts.Add(new Point(
                            curpos.X - thisNd.disp.x,
                            curpos.Y - thisNd.disp.y
                        ));
                        thisNd.beFocus = true;
                        _action = SelectActions.MovingNodes;
                    }  
                }                    
            }
        }

        private void _MovingNodesHandle()
        {
            Point curpos = Stage.CursorPos;
            for (int i = 0; i < _selNodes.Count; i++)
            {
                _selNodes[i].disp.x = (float)(curpos.X - _offsetPts[i].X);
                _selNodes[i].disp.y = (float)(curpos.Y - _offsetPts[i].Y);


                Wire[] wires = _wires.find(_selNodes[i]);
                for (int j = 0; j < wires.Length; j++)
                {
                    wires[j].redraw();
                }

            }
        }

        private void _clearAction()
        {
            for (int i = 0; i < _selNodes.Count; i++)
            {
                _selNodes[i].beFocus = false;
            }              
            _selNodes.Clear();
            _offsetPts.Clear();
            _action = SelectActions.None;
        }

        private void _EndMovingNodesHandle()
        {
            if (_selNodes.Count > 0)
            {
                _action = SelectActions.AlreadySelectedNodes;
            } else
            {
                _clearAction();
            }
        }

        private void _OnStageMouseDownHandle(InteractiveType type, Event e)
        {
            _clearAction();
            _beginPt = Stage.CursorPos;
            _selectFrame.visible = true;
            _selectFrame.erase();
            _action = SelectActions.SelectingNodes;
        }

        private void _OnStageMouseUpHandle(InteractiveType type, Event e)
        {         
            int countSelNds = _selNodes.Count;
            if (_action == SelectActions.SelectingNodes)
            {
                _EndSelectHandle();
            } else if (_action == SelectActions.MovingNodes)
            {
                _EndMovingNodesHandle();
            }
            if ( _action == SelectActions.None ) // reset focus nodes...
            {
                _clearAction();
                Point curpt = Stage.CursorPos;
                Node focusNd = null;
                foreach (Node node in _eachNodes())
                {
                    if (node.beFocus )
                    {
                        if (node.disp.hitTestPoint(curpt)
                            && (focusNd == null || node.disp.z > focusNd.disp.z))
                        {
                            focusNd = node;
                        } 
                        node.beFocus = false;
                    }                    
                }
                if (null != focusNd)
                {
                    focusNd.beFocus = true;
                    _action = SelectActions.AlreadySelectedNodes;
                    _selNodes.Add(focusNd);
                }
                else
                    _clearAction();
            }
        }

        private bool _selectAction()
        {
            int interactiveType = -1, index = -1;
            for (int i = 0; i < _interactiveReq.Count; i++ )
            {
                int type = (int)_interactiveReq[i].type;
                if (type > interactiveType)
                {
                    interactiveType = type;
                    index = i;
                }
            }
            if (index < 0)
                return false;
            _currentReq = _interactiveReq[index];
//            Logger.Instance.LogInfo("[current_req]type:{0}", _currentReq.type);
            return true;
        }


        private void _SelectingHandle()
        {
            Point min, max;
            Point curpt = Stage.CursorPos;
            Util.GetMinMaxPoint(new Point[]
                            {
                                curpt, _beginPt
                            }, out min, out max);
            float width = (float)(max.X - min.X);
            float height = (float)(max.Y - min.Y);
            _selectFrame.draw((float)min.X, (float)min.Y, width, height);       
        }

        private void _EndSelectHandle()
        {
            _selectFrame.erase();
            _selectFrame.visible = false;
            _selNodes.Clear();
            
            Point min, max;
            Point curpt = Stage.CursorPos;

            Util.GetMinMaxPoint(new Point[]
                {
                    curpt, _beginPt
                }, out min, out max);

            Boundary boundary = new Boundary();
            boundary.left = (float)min.X;
            boundary.top = (float)min.Y;
            boundary.right = (float)max.X;
            boundary.bottom = (float)max.Y;

            foreach (Node node in _eachNodes())
            {
                /*
                Boundary objboundary = node.disp.globalBoundary;
                if (
                    objboundary.p0.X > boundary.p0.X &&
                    objboundary.p0.Y > boundary.p0.Y &&
                    objboundary.p3.X < boundary.p3.X &&
                    objboundary.p3.Y < boundary.p3.Y
                )*/
                NGraphics.Point pt = node.centralPoint;
                float px = (float)pt.X,
                      py = (float)pt.Y;                
                // test central point
                if (px > boundary.p0.X &&
                    px < boundary.p3.X &&
                    py > boundary.p0.Y &&
                    py < boundary.p3.Y)
                {
                    node.beFocus = true;
                    _selNodes.Add(node);
                }
            }

            if (_selNodes.Count > 0)
            {                
                _action = SelectActions.AlreadySelectedNodes;
            }
            else
            {
                _clearAction();
            }
        }
    }
}
