﻿//#define EnableGlueEffect

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WA.GraphicLib;

namespace WA.Node
{
    public enum NodeType 
    {
        StartNode=0,
        EndNode,
        Both
    }

    public class Node
    {        
        private Sprite _disp;
        private DrawImage _img;
        private Quad[] _glues;
        private Quad _background;
        private float _halfGlueWH;
        private bool _beNdMouseDwn;
        private bool _beFocus;
        private NodeType _nodeType;

        public Node(System.Drawing.Image img, NodeType nt)
        {
            _beFocus = false;
            _disp = new Sprite();
            _background = new Quad();
            _img = new DrawImage();
            _img.draw(img);
            _glues = new Quad[] { new Quad(), new Quad() };
            int wh = (int)(_img.height / 10.0f);
            int half_wh = wh >> 1;
            int half_height = (int)(_img.height) >> 1;
            _glues[0].draw(0, 0, wh, wh);
            _glues[1].draw(0, 0, wh, wh);

            _background.x = 
            _img.x = half_wh;

            _background.alpha = 0;
            _background.beFillColor = true;
            _background.fillColor = 0xFFFFFF;
            _background.draw(0, 0, _img.width, _img.height);

            _glues[0].y =
            _glues[1].y = half_height - half_wh;
            _glues[0].x = 0;
            _glues[1].x = (int)(_img.width);

            for (int i = 0; i < _glues.Length; i++ )
            {
                _glues[i].beFillColor = true;
                _glues[i].fillColor = 0xFFFFFF;
            }

            _disp.addChild(_background);
            _disp.addChild(_img);
            _disp.addChild(_glues[0]);
            _disp.addChild(_glues[1]);

            _halfGlueWH = half_wh;

            _beNdMouseDwn = false;

            nodeType = nt;
        }

        public bool beFillBackgroundColor
        {
            get
            {
                return _background.beFillColor;
            }
            set
            {
                _background.beFillColor = value;
            }
        }

        public uint backgroundColor
        {
            get
            {
               return _background.fillColor;
            }
            set
            {
                _background.fillColor = value;
            }
        }

        public NodeType nodeType
        {
            get
            {
                return _nodeType;
            }
            set
            {
                _nodeType = value;
                if (_nodeType == NodeType.StartNode)
                {
                    _glues[0].visible = false;
                    _glues[1].visible = true;
                }
                else if (_nodeType == NodeType.EndNode)
                {
                    _glues[0].visible = true;
                    _glues[1].visible = false;
                }
                else
                {
                    _glues[0].visible = true;
                    _glues[1].visible = true;
                }
            }
        }
/*
        public bool bedrag
        {
            get
            {
                return _disp.interactive.bedrag;
            }
            set
            {
                _disp.interactive.bedrag = value;
            }
        }
*/

        public NGraphics.Point centralPoint
        {
            get
            {
                float hw = disp.width *0.5f;
                float hh = disp.height *0.5f;
                NGraphics.Point pt= disp.localToGlobal(new NGraphics.Point(disp.x, disp.y));
                pt.X += hw;
                pt.Y += hh;
                return pt;
            }
        }

        public string guid
        {
            get
            {
                return _disp.guid;
            }
        }

        public DisplayObject disp
        {
            get
            {
                return _disp;
            }
        }

        public DrawImage image
        {
            get
            {
                return _img;
            }
        }

        public bool beHitInputGlue(NGraphics.Point pt)
        {
            if (_glues[0].visible)
                return _glues[0].hitTestPoint((float)pt.X, (float)pt.Y);
            return false;
        }

        public bool beHitOutputGlue(NGraphics.Point pt)
        {
            if (_glues[1].visible)
                return _glues[1].hitTestPoint((float)pt.X, (float)pt.Y);
            return false;
        }

        public void addEventListeners()
        {
            _disp.addEventListener(EventType.MouseDown, _OnMouseDown);
            _disp.addEventListener(EventType.StageOnMouseUp, _OnMouseUp);
            _disp.addEventListener(EventType.MouseClick, _OnMouseClick);
#if EnableGlueEffect
            _disp.addEventListener(EventType.MouseEnter, _CheckGlueEnter);
            _disp.addEventListener(EventType.MouseLeave, _CheckGlueLeave);
            _disp.addEventListener(EventType.StageOnMouseMove, _CheckGlueLeave);
#endif
        }

        public void removeEventListeners()
        {
            _disp.removeEventListener(EventType.MouseDown, _OnMouseDown);
            _disp.removeEventListener(EventType.StageOnMouseUp, _OnMouseUp);
            _disp.removeEventListener(EventType.MouseClick, _OnMouseClick);
#if EnableGlueEffect
            _disp.removeEventListener(EventType.MouseEnter, _CheckGlueEnter);
            _disp.removeEventListener(EventType.MouseLeave, _CheckGlueLeave);
            _disp.removeEventListener(EventType.StageOnMouseMove, _CheckGlueLeave);
#endif
        }
#if EnableGlueEffect
        private void _CheckGlueEnter(Event evt)
        {
            float cursorx = (float)Stage.CursorPos.X;
            float cursory = (float)Stage.CursorPos.Y;
            if (_glues[0].hitTestPoint(cursorx, cursory))
            {
                _glues[0].color = 0xFF7F00;
            }
            if (_glues[1].hitTestPoint(cursorx, cursory))
            {
                _glues[1].color = 0xFF7F00;
            }
        }

        private void _CheckGlueLeave(Event evt)
        {
            float cursorx = (float)Stage.CursorPos.X;
            float cursory = (float)Stage.CursorPos.Y;
            if (!_glues[0].hitTestPoint(cursorx, cursory))
            {
                _glues[0].color = 0;
            }
            if (!_glues[1].hitTestPoint(cursorx, cursory))
            {
                _glues[1].color = 0;
            }
            _CheckGlueEnter(evt);
        }
#endif        

        private void _OnMouseClick(Event evt)
        {
            var args = new EventNodeArg();
            args.node = this;
            args.pos = Stage.CursorPos;
            args.type = NodeLinkingType.None;
            args.wire = null;
            _disp.emit(NodeEventType.OnNodeClick, args);
        }

        private void _OnMouseDown(Event evt)
        {
            float cursorx = (float)Stage.CursorPos.X;
            float cursory = (float)Stage.CursorPos.Y;
            var args = new EventNodeArg();

            bool behitglues = false;
            for (int i = 0; i < _glues.Length; i++)
            {
                if (_glues[i].hitTestPoint(cursorx, cursory))
                {
                    behitglues = true;
                    args.node = this;
                    args.pos = i == 0 ? glueInputPos:glueOutputPos ;
                    args.type = i == 0 ? NodeLinkingType.InputToOutput : NodeLinkingType.OutputToInput;
                    _disp.emit(NodeEventType.OnNodeStartLinking, args);
                }
            }
            if (!behitglues)
            {
                _beNdMouseDwn = true;
                args.node = this;
                args.type = NodeLinkingType.None;
                args.pos = Stage.CursorPos;
                _disp.emit(NodeEventType.OnNodeMouseDown, args);
            }
        }

        public NGraphics.Point glueInputPos
        {
            get
            {
                return _glues[0].localToGlobal(new NGraphics.Point(_glues[0].x + _halfGlueWH, _glues[0].y + _halfGlueWH));
            }
        }

        public NGraphics.Point glueOutputPos
        {
            get
            {
                return _glues[1].localToGlobal(new NGraphics.Point(_glues[1].x + _halfGlueWH, _glues[1].y + _halfGlueWH));
            }
        }

        public bool beFocus
        {
            get
            {
                return _beFocus;
            }
            set
            {
                _beFocus = value;
                if (_beFocus)
                {
                    _img.border = 3.0f;
                    _img.color = 0xFF7F00;
                }
                else
                {
                    _img.border = 1;
                    _img.color = 0;
                }
            }
        }

        private void _OnMouseUp(Event evt)
        {
            var args = new EventNodeArg()
            {
                node = this,
                pos = Stage.CursorPos
            };
            _disp.emit(NodeEventType.OnNodeEndLinking, args);
            if (_beNdMouseDwn)
            {
                _beNdMouseDwn = false;
                _disp.emit(NodeEventType.OnNodeMouseUp, args);
            }
        }
    }
}
