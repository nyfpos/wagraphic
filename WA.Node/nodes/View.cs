﻿using NGraphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WA.GraphicLib;

namespace WA.Node
{
    internal enum ViewActions
    {
        None = 0,
        StartLinking,
        CheckLinked
    }

    public class View :EventDispatcher
    {        
        private Stage _stage;

        private List<Node> _nodes;        

        private ViewActions _viewActions;
        private NGraphics.Point _linkingPt;
        private Node _linkingNd;
        private DrawPath _linkingPath;
        private NodeLinkingType _linkingType;
        private bool _bedrag;        

        private Wires _wires;
        private Wire _focusWire;        

        private SelectHelper _selHelper;

        public View(Stage stage)
        {            
            _stage = stage;            
            _nodes = new List<Node>();            
            _viewActions = ViewActions.None;
            _bedrag = false;
            _linkingPt = NGraphics.Point.Zero;
            _linkingNd = null;
            _focusWire = null;
            _linkingPath = new DrawPath();
            _stage.add(_linkingPath);

            _linkingPath.border = 3.0f;
            _linkingPath.color = 0xFF7F00;

            _linkingType = NodeLinkingType.None;
            _wires = new Wires();
            _wires.addEventListener(NodeEventType.OnNodeWireClick, _OnWireClick);

            _selHelper = new SelectHelper(_stage, _wires, eachNodes);
            _selHelper.start();
        }

        public bool getFocusItem(out Node[] nodes, out Wire wire)
        {
            nodes = _selHelper.getFocusNodes();
            wire = _focusWire;
            if (nodes.Length == 0 ||
                wire == null)
                return false;
            return true;
        }

        public IEnumerable<Node> eachNodes()
        {
            for (int i =0; i < _nodes.Count; i++)
            {
                yield return _nodes[i];
            }
        }

        public void start()
        {
            bedrag = true;
            _stage.addEventListener(EventType.StageOnUpdate, _OnUpdate);
            _stage.addEventListener(EventType.StageOnMouseDown, _OnStageMouseDown);
        }

        public void stop()
        {
            bedrag = false;
            _stage.removeEventListener(EventType.StageOnUpdate, _OnUpdate);
            _stage.removeEventListener(EventType.StageOnMouseDown, _OnStageMouseDown);
        }

        public void deleteItem(params object[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                object obj = args[i];
                Wire w = obj as Wire;
                Node n = obj as Node;
                Node[] ns = obj as Node[];
                deleteItem(w);
                deleteItem(n);
                deleteItem(ns);
            }
        }

        public void deleteItem(Wire wire)
        {
            if (wire != null)
            {
                _wires.remove(wire);
                _stage.remove(wire.disp);
            }
        }

        public void deleteItem(Node[] nodes)
        {
            if (nodes != null)
            {
                for (int i = 0; i < nodes.Length; i++)
                {
                    if (null == nodes[i])
                        continue;
                    remove(nodes[i]);
                }
            }
        }

        public void deleteItem(Node node)
        {
            if (node != null)
            {
                remove(node);
            }
        }

        private void _OnStageMouseDown(Event e)
        {
            MouseEventArgs arg = (MouseEventArgs)e.data;
            if ( !_wires.hasFocusAndClearFocus(new NGraphics.Point(arg.X, arg.Y)) )
            {
                _focusWire = null; // no more focus wire
            }
        }

        public bool bedrag
        {
            get
            {
                return _bedrag;
            }
            set
            {
                _bedrag = value;
                _selHelper.bedrag = _bedrag;
            }
        }

        public void add(Node node)
        {
            if (!_nodes.Contains(node))
            {
                _nodes.Add(node);
                node.addEventListeners();
                node.disp.addEventListener(NodeEventType.OnNodeStartLinking, _OnNodeStartLinking);
                node.disp.addEventListener(NodeEventType.OnNodeEndLinking, _OnNodeEndLinking);
                node.disp.addEventListener(NodeEventType.OnNodeMouseDown, _OnNodeMouseDown);                                             
                _stage.add(node.disp);
            }
        }

        private void _OnNodeMouseDown(Event e)
        {
            _selHelper.OnNodeMouseDown(e);
        }

        private void _OnNodeStartLinking(Event e)
        {
            bedrag = false;
            _wires.clearFocusWire();
            EventNodeArg arg = (EventNodeArg)e.data;
            _linkingPt = arg.pos;
            _linkingNd = arg.node;
            _linkingType = arg.type;
            _viewActions = ViewActions.StartLinking;
            _selHelper.OnNodeGlueMouseDown(e);
        }

        private void _OnWireClick(Event evt)
        {
            EventNodeArg arg = (EventNodeArg)evt.data;
            _focusWire = arg.wire;
            _selHelper.OnWireMouseDown(evt);
        }

        private void _OnNodeEndLinking(Event e)
        {
            bedrag = true;
            _viewActions = ViewActions.CheckLinked;
        }

        public void linkNodes(Node parent, Node child)
        {
            Wire wire = _wires.link(parent, child);
            if (null != wire)
                _stage.add(wire.disp);            
        }

        public void remove(Node node)
        {
            if (_nodes.Remove(node))
            {                
                node.removeEventListeners();
                node.disp.removeEventListener(NodeEventType.OnNodeStartLinking, _OnNodeStartLinking);
                node.disp.removeEventListener(NodeEventType.OnNodeEndLinking, _OnNodeEndLinking);
                node.disp.removeEventListener(NodeEventType.OnNodeMouseDown, _OnNodeMouseDown);
                _stage.remove(node.disp);
                Wire[] ws = _wires.find(node);
                for (int i = 0; i < ws.Length; i++)
                {
                    var wire = ws[i];
                    _wires.remove(wire);
                    _stage.remove(wire.disp);
                }
            }
        }

        private bool _beHitOtherNode(NodeLinkingType linkType, Node ignoreNode, NGraphics.Point pt, out Node hitNode)
        {
            hitNode = null;
            for (int i = 0; i < _nodes.Count; i++)
            {
                Node node = _nodes[i];

                if (linkType == NodeLinkingType.OutputToInput &&
                    node.nodeType == NodeType.StartNode) // the node do not have input glue...
                    continue;

                if (linkType == NodeLinkingType.InputToOutput &&
                    node.nodeType == NodeType.EndNode) // the node do not have output glue...
                    continue;

                if (node.guid != ignoreNode.guid && 
                    node.disp.hitTestPoint((float)pt.X, (float)pt.Y))
                {
                    hitNode = node;
                    return true;
                }
            }
            return false;
        }

        private void _OnUpdate(Event e)
        {
            if (_viewActions == ViewActions.StartLinking)
            {
                if (_linkingNd == null)
                {
                    bedrag = true;                    
                    _linkingNd = null;
                    _viewActions = ViewActions.None;
                    return;
                }

                NGraphics.Point st_pt = _linkingPt;
                NGraphics.Point ed_pt = Stage.CursorPos;

                float nw = (int)_linkingNd.disp.width,
                      nh = 3;
                _linkingPath.visible = true;
                Wire.drawLink(_linkingPath, st_pt, ed_pt, nw, nh,
                    _linkingType == NodeLinkingType.OutputToInput ? 1:-1);
            }
            else if (_linkingNd != null && _viewActions == ViewActions.CheckLinked)
            {
                NGraphics.Point ed_pt = Stage.CursorPos;
                for (int i = 0; i < _nodes.Count; i++)
                {
                    if (_linkingNd.guid == _nodes[i].guid)
                        continue;
                    if (_linkingType == NodeLinkingType.OutputToInput)
                    {
                        if (_nodes[i].beHitInputGlue(ed_pt))
                        {
                            Wire wire = _wires.link(_linkingNd, _nodes[i]);
                            if (null != wire)
                                _stage.add(wire.disp);
                            break;
                        }
                        Node hitnd;
                        if (_beHitOtherNode(_linkingType, _linkingNd, ed_pt, out hitnd))
                        {                            
                            Wire wire = _wires.link(_linkingNd, hitnd);
                            if (null != wire)
                                _stage.add(wire.disp);
                            break;
                        }
                    } else if (_linkingType == NodeLinkingType.InputToOutput)
                    {
                        if (_nodes[i].beHitOutputGlue(ed_pt))
                        {
                            Wire wire = _wires.link(_nodes[i], _linkingNd);
                            if (null != wire)
                                _stage.add(wire.disp);
                            break;
                        }
                    }
                }
                _linkingPath.erase();
                _linkingPath.visible = false;
                _linkingNd = null;
                _linkingType = NodeLinkingType.None;
                _viewActions = ViewActions.None;
            }
            else
            {

            }
        }

    }
}
