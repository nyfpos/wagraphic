﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.Node
{    
    public enum NodeLinkingType
    {
        None=0,
        OutputToInput,
        InputToOutput
    }

    public enum ItemType
    {
        Node=0,
        Wire
    }

    public struct EventNodeArg
    {
        public Node node;
        public NGraphics.Point pos;
        public NodeLinkingType type;
        public Wire wire;
    }

    public struct EventItemFocusArg
    {
        public ItemType itemType;
        public Node[] nodes;
        public Wire wire;
    }


    public struct NodeEventType
    {
        static public readonly string OnNodeStartLinking = "OnNodeStartLinking";
        static public readonly string OnNodeEndLinking = "OnNodeEndLinking";
        static public readonly string OnNodeMouseDown = "OnNodeMouseDown";
        static public readonly string OnNodeMouseUp = "OnNodeMouseUp";
        static public readonly string OnNodeWireClick = "OnNodeWireClick";
        static public readonly string OnNodeClick = "OnNodeClick";


    //    static public readonly string OnItemFocus = "OnItemFocus";
    }
}
