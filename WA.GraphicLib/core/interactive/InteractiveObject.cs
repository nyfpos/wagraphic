﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public class InteractiveObject
    {
        private bool _startDrag;
        private DisplayObject _disp;
        private NGraphics.Point _offsetPt;
        private bool _bedrag;
        private bool _enable;

        public InteractiveObject(DisplayObject disp)
        {
            _disp = disp;
            _bedrag = false;
            _enable = false;
            _startDrag = false;
            _offsetPt = NGraphics.Point.Zero;
        }

        public bool enable
        {
            get
            {
                return _enable;
            }
            set
            {
                _enable = value;
                if (_enable)
                    _addEventListeners();
                else
                    _removeEventListener();
            }
        }

        public bool bedrag
        {
            get
            {
                return _bedrag;
            }
            set
            {
                _bedrag = value;
                _updateEnableFlags();
            }
        }

        private void _updateEnableFlags()
        {
            enable = bedrag;
        }

        private void _addEventListeners()
        {
            _disp.addEventListener(EventType.MouseDown, _OnMouseDown);
            _disp.addEventListener(EventType.MouseUp, _OnMouseUp);
            _disp.addEventListener(EventType.MouseLeave, _OnMouseLeave);
            _disp.addEventListener(EventType.StageOnMouseLeave, _OnMouseUp);
            _disp.addEventListener(EventType.StageOnMouseMove, _OnMouseMove);
        }

        private void _removeEventListener()
        {
            _disp.removeEventListener(EventType.MouseDown, _OnMouseDown);
            _disp.removeEventListener(EventType.MouseUp, _OnMouseUp);
            _disp.removeEventListener(EventType.MouseLeave, _OnMouseLeave);
            _disp.removeEventListener(EventType.StageOnMouseLeave, _OnMouseUp);
            _disp.removeEventListener(EventType.StageOnMouseMove, _OnMouseMove);
        }

        private void _OnMouseUp(Event evt)
        {
            _startDrag = false;
            _offsetPt = NGraphics.Point.Zero;     
        }

        private void _OnMouseLeave(Event evt)
        {
            MouseLeaveReason reason = (MouseLeaveReason)evt.data;
            if (reason == MouseLeaveReason.UnderOtherObjects)
            {
                _startDrag = false;
                _offsetPt = NGraphics.Point.Zero;
            }
        }

        private void _OnMouseDown(Event evt)
        {
            if (bedrag)
            {
                var cursor = Stage.CursorPos;
                _offsetPt.X = (float)(cursor.X - _disp.x);
                _offsetPt.Y = (float)(cursor.Y - _disp.y);
                _startDrag = true;
            }
        }

        private void _OnMouseMove(Event evt)
        { 
            if (_startDrag)
            {
                var cursor = Stage.CursorPos;
                _disp.x = (float)(cursor.X - _offsetPt.X);
                _disp.y = (float)(cursor.Y - _offsetPt.Y);
            }
        }
    }
}
