﻿using NGraphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{

    public class DrawPath : DisplayObject
    {
        protected bool _beUpdateBoundary;

        private bool _beDraw;         
        private List<Point> _pts;
        private IList<PathOp> _operations;

        private const float MinRange = 4;
        private const float HalfMinRange = 2;

        public DrawPath()
        {
            _pts = new List<Point>();            
            _beDraw = false;
            _beUpdateBoundary = false;
        }

        private void _UpdateDrawBoundary()
        {
            _pts.Clear();
            Point prev_pt = Point.Zero;
            for (int i = 0; i < _operations.Count; i++)
            {
                PathOp op = _operations[i];
                if (op is CurveTo)
                {                    
                    var cv = op as CurveTo;
                    for (int j = 0; j < 11; j++)
                    {
                        float t = (float)j / 10.0f;
                        _pts.Add(Util.ApproximateBezier(prev_pt, cv.Control1, cv.Control2, cv.Point, t));
                    }
                }
                else
                {
                    _pts.Add(op.EndPoint);
                }
                prev_pt = op.EndPoint;
            }

            Point min, max;
            Util.GetMinMaxPoint(_pts.ToArray(), out min, out max);

            _drawBoundary.left = (float)min.X;
            _drawBoundary.top = (float)min.Y;
            _drawBoundary.right = (float)max.X;
            _drawBoundary.bottom = (float)max.Y;

            float dx = _drawBoundary.right - _drawBoundary.left;
            float dy = _drawBoundary.bottom - _drawBoundary.top;

            if (dx < MinRange)
            {
                _drawBoundary.left -= HalfMinRange;
                _drawBoundary.right += HalfMinRange;
            }
            if (dy < MinRange)
            {
                _drawBoundary.top -= HalfMinRange;
                _drawBoundary.bottom += HalfMinRange;
            }
//            Logger.Instance.LogInfo("dw:{0} dh:{1}",_drawBoundary.dw,_drawBoundary.dh);
        }

        public IEnumerable eachBoundaryPoint()
        {
            for (int i = 0; i < _pts.Count; i++)
            {
                yield return localToGlobal(_pts[i]);
            }
        }

        public int countBoundaryPoint()
        {
            return _pts.Count;
        }

        public virtual void draw(IList<PathOp> operations)
        {
            _operations = operations;
            if (_operations != null &&
                _operations.Count > 0)
            {
                _UpdateDrawBoundary();
                emit(EventType.ReDraw, null);
                _beChanged = true;
                _beDraw = true;
                _beUpdateBoundary = true;
            }
        }

        internal override void afterRender()
        {
            base.afterRender();
            _beUpdateBoundary = false;
        }

        public void erase()
        {
            _operations = null;
            _beDraw = false;
            _beChanged = true;

            _drawBoundary.left =
            _drawBoundary.top =
            _drawBoundary.right =
            _drawBoundary.bottom = 0;
        }

        internal override void render(GraphicsCanvasEx canvas, float deltaTime)
        {
            if (!_beDraw)
                return;

            Brush fillBrush;
            Pen pen;
            setupBrushAndPen(out fillBrush, out pen);

            // scale*rotation*translation....
            applyTransform(canvas);

            if (beFillColor)
            {
                canvas.FillPath(_operations, fillBrush);
            }
            canvas.DrawPath(_operations, pen, brush);
        }
    }
}
