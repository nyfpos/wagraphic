﻿using NGraphics;
using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    abstract public class DisplayObject : EventDispatcher
    {
        private DisplayObjectContainer _parent;

        private float _border;           
        private bool _beVisible;
        private float _alpha;
        private uint _color;
        private NGraphics.Brush _brush;
        private float[] _dashPattern;

        private bool _beFillColor;
        private uint _fillColor;
        private float _fillAlpha;

        private LocalTransform _localTrans;
        
        static protected bool _beChanged;
        protected Boundary _drawBoundary;

        private InteractiveObject _interactive;

        public DisplayObject()
        {
            _border = 0.0f;
            _color = 0;
            _alpha = 1.0f;
            _parent = null;                        
            _beVisible = true;
            _beChanged = true;            
            _brush = null;            
            _beFillColor = false;
            _fillColor = 0;
            _fillAlpha = 1.0f;
            _localTrans = new LocalTransform(this);
            _drawBoundary = new Boundary();
            _interactive = new InteractiveObject(this);
            guid = Guid.NewGuid().ToString();
        }

        public object data
        {
            get;
            set;
        }

        public InteractiveObject interactive
        {
            get
            {
                return _interactive;
            }
        }

        internal void clearEventSettings()
        {
            mousedown = false;
            hovered = false;
        }
        internal bool mousedown { get; set; }
        internal bool hovered { get; set; }

        internal Boundary drawBoundary
        {
            get
            {
                return _drawBoundary;
            }
        }

        internal LocalTransform local
        {
            get
            {
                return _localTrans;
            }
        }

        public string guid
        {
            internal set;
            get;
        }

        public bool beFillColor
        {
            get
            {
                return _beFillColor;
            }
            set
            {
                if (value != _beFillColor)
                {
                    _beFillColor = value;
                    _beChanged = true;
                }
            }
        }

        public float fillAlpha
        {
            get
            {
                return _fillAlpha;
            }
            set
            {
                if (value != _fillAlpha)
                {
                    _fillAlpha = value;
                    _beChanged = true;
                }
            }
        }

        public uint fillColor
        {
            get
            {
                return _fillColor;
            }
            set
            {
                if (value != _fillColor)
                {
                    _fillColor = value;
                    _beChanged = true;
                }
            }
        }

        public uint color
        {
            get
            {
                return _color;
            }
            set
            {
                if (value != _color)
                {
                    _color = value;
                    _beChanged = true;
                }
            }
        }

        public float alpha
        {
            get
            {
                return _alpha;
            }
            set
            {
                if (value != _alpha)
                {
                    _alpha = value;
                    _beChanged = true;
                }
            }
        }

        public NGraphics.Brush brush
        {
            get
            {
                return _brush;
            }
            set
            {
                if (value != _brush)
                {
                    _brush = value;
                    _beChanged = true;
                }
            }
        }

        public string name
        {
            get;
            set;
        }

        // local transform + draw
        public virtual float x
        {
            get
            {
                return _drawBoundary.left + local.x; // boundary.left+transform.x = x
            }
            set
            {
                if ((_drawBoundary.left + local.x) != value)
                {
                    local.x = value - _drawBoundary.left; // transform.x = x-boundary.left
                    _beChanged = true;
                }
            }
        }

        // local transform + draw
        public virtual float y
        {
            get
            {
                return _drawBoundary.top + local.y; // boundary.top+transform.y = y
            }
            set
            {
                if ((_drawBoundary.top + local.y) != value)
                {
                    local.y = value - _drawBoundary.top; // transform.y = y-boundary.top
                    _beChanged = true;
                }
            }
        }

        // local transform 
        public float z
        {
            get
            {
                return local.z;
            }
            set
            {
                if (local.z != value)
                {
                    local.z = value;
                    _beChanged = true;
                }
            }
        }

        public float border
        {
            get
            {
                return
                    _border;
            }
            set
            {
                if (value != _border)
                {
                    _border = value;
                    _beChanged = true;
                }
            }
        }

        public float[] dashPattern
        {
            get
            {
                return _dashPattern;
            }
            set
            {
                if (value != _dashPattern)
                {
                    _dashPattern = value;
                    _beChanged = true;
                }
            }
        }

        // local transform 
        // degree
        public float rotateZ
        {
            get
            {
                return local.rotateZ;
            }
            set
            {
                if (local.rotateZ != value)
                {
                    local.rotateZ = value;
                    _beChanged = true;
                }
            }
        }

        // local transform 
        public float scaleX
        {
            get
            {
                return local.scaleX;
            }
            set
            {
                if (local.scaleX != value)
                {
                    local.scaleX = value;
                    _beChanged = true;
                }
            }
        }

        // local transform 
        public float scaleY
        {
            get
            {
                return local.scaleY;
            }
            set
            {
                if (local.scaleY != value)
                {
                    local.scaleY = value;
                    _beChanged = true;
                }
            }
        }

        // local transform + draw
        public virtual float width
        {
            get
            {                
                return _drawBoundary.dw * local.scaleX;
            }
            set
            {
                if (value != _drawBoundary.dw*local.scaleX)
                {
                    // w = dw*scaleX
                    if (_drawBoundary.dw>0)
                    {
                       local.scaleX= value / _drawBoundary.dw;
                       _beChanged = true;
                    }                    
                }
            }
        }

        // local transform + draw
        public virtual float height
        {
            get
            {                
                return _drawBoundary.dh * local.scaleY;
            }
            set
            {
                if (value != _drawBoundary.dh*local.scaleY)
                {
                    // h = dh*scaleY
                    if (_drawBoundary.dh>0)
                    {
                        local.scaleY = value / _drawBoundary.dh;
                        _beChanged = true;
                    }                    
                }
            }
        }

        // global
        public virtual Boundary globalBoundary
        {
            get
            {
                Point minXY, maxXY;
                Transform transform = getTransform();
                Point[] points= new Point[] {
                                transform.TransformPoint(_drawBoundary.p0),
                                transform.TransformPoint(_drawBoundary.p1),
                                transform.TransformPoint(_drawBoundary.p2),
                                transform.TransformPoint(_drawBoundary.p3)
                };
                Util.GetMinMaxPoint(points, out minXY, out maxXY);
                return new Boundary()
                {
                    left = (float)minXY.X,
                    top = (float)minXY.Y,
                    right = (float)maxXY.X,
                    bottom = (float)maxXY.Y
                };
            }
//          set;
        }

        public NGraphics.Point localToGlobal(NGraphics.Point pt)
        {            
            DisplayObject disp = this;
            if (disp .parent!= null)
            {
                pt = disp.parent.global.toMatrix().TransformPoint(pt);
            }
            return new Point((int)pt.X, (int)pt.Y);
        }

        public bool visible
        {
            get
            {
                return _beVisible;
            }
            set
            {
                if (value != _beVisible)
                {
                    _beVisible = value;
                    _beChanged = true;
                }
            }
        }

        public DisplayObjectContainer parent
        {
            get
            {
                return _parent;
            }
            internal set
            {
                _parent = value;
            }
        }

        public Stage stage
        {
            get;
            internal set;
        }

        public virtual bool hitTestPoint(NGraphics.Point pt)
        {
            return hitTestPoint((float)pt.X, (float)pt.Y);
        }

        public virtual bool hitTestPoint(float px, float py)
        {
            Boundary boundary = globalBoundary;
            return (px > boundary.p0.X &&
                    px < boundary.p3.X &&
                    py > boundary.p0.Y &&
                    py < boundary.p3.Y);
        }

        public virtual bool hitTestObject(DisplayObject obj)
        {
            Boundary objboundary = obj.globalBoundary;
            Boundary boundary = globalBoundary;
            return !(objboundary.left > boundary.right ||
                     objboundary.right < boundary.left ||
                     objboundary.top > boundary.bottom ||
                     objboundary.bottom < boundary.top);
        }

        public virtual bool containTestObject(DisplayObject obj)
        {
            Boundary objboundary = obj.globalBoundary;
            Boundary boundary = globalBoundary;
            return (
                    objboundary.p0.X > boundary.p0.X &&
                    objboundary.p0.Y > boundary.p0.Y &&
                    objboundary.p3.X < boundary.p3.X &&
                    objboundary.p3.Y < boundary.p3.Y                     
                );
        }

        internal Transform getTransform()
        {
            // scale*rotation*translation....
            Transform transform = Transform.Identity;
            if (parent != null)
                transform = Stage.worldTransform * parent.global.toMatrix() * local.toMatrix();
            else
                transform = Stage.worldTransform * local.toMatrix();
            return transform;
        }

        public void applyTransform(GraphicsCanvasEx canvas)
        {
            canvas.ResetTransform();
            canvas.Transform(getTransform());
        }

        public void setupBrushAndPen(out  Brush fillBrush, out  Pen pen)
        {
            // create pen & brush & transform....
            fillBrush = Util.HexColorToBrushNG(fillColor, fillAlpha);
            if (brush != null)
            {
                fillBrush = brush;
            }
            pen = new Pen(Util.HexColorNG(color, alpha), border);
            pen.DashPattern = _dashPattern;
        }

        internal virtual void beforeRender()
        {
            if (_beChanged)
            {
                if (visible)
                    emit(EventType.Change, null);                
            }
        }

        internal virtual void afterRender()
        {
            _beChanged = false;
        }

        abstract internal void render(GraphicsCanvasEx canvas, float deltaTime);
    }
}
