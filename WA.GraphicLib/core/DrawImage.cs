﻿using NGraphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public class DrawImage : DisplayObject
    {
        private bool _beDraw;
        private NGraphics.IImage _img;

        public DrawImage()
        {
            _beDraw = false;
        }

        public void draw(System.Drawing.Image image)
        {
            _img = new NGraphics.ImageImage(image);
            _beDraw = true;
            _drawBoundary.left = 0;
            _drawBoundary.top = 0;
            _drawBoundary.right = image.Width;
            _drawBoundary.bottom = image.Height;
            _beChanged = true;
            emit(EventType.ReDraw, null);
        }

        internal override void render(GraphicsCanvasEx canvas, float deltaTime)
        {
            if (!_beDraw)
                return;

            // create pen & brush & transform....
            Brush fillBrush;
            Pen pen;
            setupBrushAndPen(out fillBrush, out pen);

            // scale*rotation*translation....
            applyTransform(canvas);
            canvas.DrawImage(_img);
            canvas.DrawRectangle(_drawBoundary.left, _drawBoundary.top, _drawBoundary.dw, _drawBoundary.dh, pen, brush);
        }
    }
}
