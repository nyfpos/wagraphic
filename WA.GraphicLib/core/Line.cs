﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NGraphics;

namespace WA.GraphicLib
{
    public class Line : DisplayObject
    {
        private bool _beDraw;
        private Point _start;
        private Point _end;
        
        public Line()
        {
            _beDraw = false;
        }

        public void erase()
        {
            _beDraw = false;
            _beChanged = true;
            _drawBoundary.left =
            _drawBoundary.top =
            _drawBoundary.right =
            _drawBoundary.bottom = 0;
        }

        public void draw(Point start, Point end)
        {            
            Point[] pts = new Point[]
            {
              start, end
            };

            Point min, max;
            Util.GetMinMaxPoint(pts, out min, out max);            
            _drawBoundary.left = (float)min.X;
            _drawBoundary.top = (float)min.Y;
            _drawBoundary.right = (float)max.X;
            _drawBoundary.bottom = (float)max.Y;

            _start = pts[0];
            _end = pts[1];
            _beDraw = true;
            _beChanged = true;
            emit(EventType.ReDraw, null);
        }

        internal override void render(GraphicsCanvasEx canvas, float deltaTime)
        {
            if (!_beDraw)
                return;

            // create pen & brush & transform....            
            Pen pen = new Pen(Util.HexColorNG(color, alpha));
            pen.DashPattern = dashPattern;
            // scale*rotation*translation....
            applyTransform(canvas);
            canvas.DrawLine(_start, _end, pen);
        }
    }
}
