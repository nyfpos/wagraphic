﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    abstract public class DisplayObjectContainer : DisplayObject
    {
        protected List<DisplayObject> _objects;

        private List<NGraphics.Point> _tmpPoints;
        private GlobalTransform _globalTrans;        

        public DisplayObjectContainer()
        {
            _objects = new List<DisplayObject>();
            _tmpPoints = new List<NGraphics.Point>();
            _globalTrans = new GlobalTransform(this);
        }

        private void _OnChanged(Event evt)
        {
            _beChanged = true;
        }

        internal GlobalTransform global
        {
            get
            {
                return _globalTrans;
            }
        }

        public NGraphics.Point pivot
        {
            get
            {
                return _globalTrans.pivot;
            }
            set
            {
                if (value != _globalTrans.pivot)
                {
                    _globalTrans.pivot = value;
                    _beChanged = true;
                }
            }
        }

        // local transform
        public override float x
        {
            get
            {
                return local.x;
            }
            set
            {
                if (value != local.x)
                {
                    local.x = value;
                    _beChanged = true;                    
                }
            }
        }

        // local transform
        public override float y
        {
            get
            {
                return local.y;
            }
            set
            {
                if (value != local.y)
                {
                    local.y = value;
                    _beChanged = true;
                }
            }
        }

        public override float width
        {
            get
            {
                return globalBoundary.dw;
            }
            set
            {
                
            }
        }

        public override float height
        {
            get
            {                
                return globalBoundary.dh;
            }
            set
            {
                
            }
        }

        public int numChildren
        {
            get 
            {
                return _objects.Count;
            }
        }

        public bool addChild(DisplayObject obj)
        {
            // TODO: IF there is object's parent. obj.parent.emit(EventType.RemoveChild...
            if (_objects.IndexOf(obj) < 0)
            {
                if (obj.parent != null)
                {
                    obj.parent.removeChild(obj);
                }
                if (obj.stage != null)
                {
                    obj.stage.remove(obj);                    
                }
                _objects.Add(obj);
                obj.parent = this;
                return true;
            }
            return false;
        }

        public bool addChildAt(DisplayObject obj, int index)
        {
            if (_objects.IndexOf(obj) < 0)
            {
                try
                {
                    if (obj.parent != null)
                    {
                        obj.parent.removeChild(obj);
                    }
                    if (obj.stage != null)
                    {
                        obj.stage.remove(obj);
                    }
                    _objects.Insert(index, obj);
                    obj.parent = this;
                    return true;
                }
                catch (Exception ex)
                {
                    Logger.Instance.Error(ex.Message);
                    Logger.Instance.Debug(ex.StackTrace);
                    return false;
                }
            }
            return false ;
        }

        public int getChildIndex(DisplayObject obj)
        {
            return _objects.IndexOf(obj);
        }

        public DisplayObject getChildAt(int index)
        {
            if (index >= 0 && index < _objects.Count)
                return _objects[index];
            return null;
        }

        public DisplayObject getChildByName(string name)
        {
            for (int i = 0; i < _objects.Count; i++)
            {
                if (_objects[i].name == name)
                    return _objects[i];
            }
            return null;
        }

        public bool removeChild(DisplayObject child)
        {            
            if (_objects.Remove(child))
            {
                child.parent = null;
                return true;
            }
            return false;
        }

        public bool removeChildAt(int index)
        {
            try
            {
                if (index < _objects.Count && index >= 0)
                {
                    DisplayObject obj = _objects[index];
                    _objects.Remove(obj);
                    obj.parent = null;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.Instance.Error(ex.Message);
                Logger.Instance.Debug(ex.StackTrace);
                return false;
            }
            return true;
        }

        public IEnumerable<DisplayObject> each()
        {
            for (int i = 0; i < _objects.Count; i++)
            {
                yield return _objects[i];
            }
        }

        public void clear()
        {
            while (_objects.Count > 0)
            {
                removeChild(_objects[0]);
            }
        }

        public override Boundary globalBoundary
        {
            get
            {
                _tmpPoints.Clear();
                foreach (DisplayObject dispobj in each())
                {
                    Boundary boundary = dispobj.globalBoundary;
                    var p1 = new NGraphics.Point(boundary.left,
                                                 boundary.top);
                    var p2 = new NGraphics.Point(boundary.right,
                                                 boundary.bottom);
                    _tmpPoints.Add(p1);
                    _tmpPoints.Add(p2);
                }

                NGraphics.Point min, max;
                Util.GetMinMaxPoint(_tmpPoints.ToArray(), out min, out max);

                return new Boundary()
                {
                    left = (float)min.X,
                    top = (float)min.Y,
                    right = (float)max.X,
                    bottom = (float)max.Y,
                };
            }
            //          set;
        }

    }
}
