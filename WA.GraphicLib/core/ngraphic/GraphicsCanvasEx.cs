﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public class GraphicsCanvasEx : NGraphics.GraphicsCanvas
    {
        public GraphicsCanvasEx(Graphics graphics)
            : base(graphics)
        {

        }

        public void ResetTransform()
        {
            graphics.ResetTransform();
        }

        public void Clear(Color color)
        {
            graphics.Clear(color);
        }
    }
}
