﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public class Sprite : DisplayObjectContainer
    {        
        private Quad _quad;
        private Quad _spriteFrame;
        private bool _showBoundary;
        private bool _showFrame;

        public Sprite()
        {
            _spriteFrame = new Quad();
            _quad = new Quad();
            _showBoundary = false;
            _showFrame = false;
           // addEventListener(EventType.ReDraw, _OnDrawBoundaryQuad);
        }

        public bool showFrame
        {
            get
            {
                return _showFrame;
            }
            set
            {
                _showFrame = value;
            }
        }

        public bool showBoundary
        {
            get
            {
                return _showBoundary;
            }
            set
            {
                _showBoundary = value;
              //  if (_showBoundary)
              //      _OnDrawBoundaryQuad(null);
            }
        }
        /*
        private void _OnDrawBoundaryQuad(Event evt)
        {            
            Boundary bound = globalBoundary;
            _quad.draw(bound.left, bound.top, bound.right - bound.left, bound.bottom - bound.top);            
        }
        */
        private int _sortRule(DisplayObject obj1, DisplayObject obj2)
        {
            return obj1.z.CompareTo(obj2.z);
        }

        internal override void render(GraphicsCanvasEx graphic, float deltaTime)
        {
            float maxw = float.MinValue, maxh = float.MinValue,
                  maxx = float.MinValue, maxy = float.MinValue;
            _objects.Sort(_sortRule);
            for (int i = 0; i < _objects.Count; i++)
            {
                if (_objects[i].visible)
                {
                    if (_objects[i].x > maxx)
                        maxx = _objects[i].x;
                    if (_objects[i].y > maxy)
                        maxy = _objects[i].y;
                    if (_objects[i].drawBoundary.dw > maxw)
                        maxw = _objects[i].width;
                    if (_objects[i].drawBoundary.dh > maxh)
                        maxh = _objects[i].height;
                    _objects[i].render(graphic, deltaTime);
                }
            }

            if (_showBoundary || _showFrame)
            {                
                if (_showFrame)
                {
                    _spriteFrame.draw(0, 0, maxx + maxw, maxy + maxh);
                    _spriteFrame.parent = parent;
                    _spriteFrame.color = 0x0000FF;
                    _spriteFrame.x = x;
                    _spriteFrame.y = y;
                    _spriteFrame.scaleX = scaleX;
                    _spriteFrame.scaleY = scaleY;
                    _spriteFrame.rotateZ = rotateZ;
                    _spriteFrame.render(graphic, deltaTime);
                }
                if (_showBoundary)
                {
                    Boundary bound = globalBoundary;
                    _quad.draw(bound.left, bound.top, bound.right - bound.left, bound.bottom - bound.top);
                    _quad.parent = null;
                    _quad.color = 0xFF0000;
                    _quad.render(graphic, deltaTime);
                }
            }
        }
    }
}
