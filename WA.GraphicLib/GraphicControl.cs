﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WA.GraphicLib
{
    public partial class GraphicControl : UserControl
    {
        public GraphicControl()
        {
            InitializeComponent();
        }

        private void GraphicControl_Load(object sender, EventArgs e)
        {
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
//          SetStyle(ControlStyles.DoubleBuffer, true);
            UpdateStyles();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }
        protected override void WndProc(ref Message m)
        {
            const int WM_ERASEBKGND = 0x14;
            if (m.Msg != WM_ERASEBKGND)
                base.WndProc(ref m);
            //  else
            //      MessageBox.Show("WM_ERASEBKGND");
        }
    }
}
