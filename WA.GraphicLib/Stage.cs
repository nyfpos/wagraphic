﻿using NGraphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WA.GraphicLib
{
    public class Stage : EventDispatcher
    {
        static private UserControl _ctrl;
        
        private EventTrigger _dispEventTrigger;
        private float _during;
        private Stopwatch _sw;        

        private Bitmap _showBuff;
        private Bitmap _backBuff;

        private List<DisplayObject> _dispObjs;
        private float _lastZ;

        static private Transform _worldTransform;

        private bool _beChanged;

        private float _alpha;
        private uint _color;
        private float _zoom;
        private bool _beRefresh;

        private Timer _timer;

        public float renderUpdateTime { get; set; }

        public Stage(UserControl ctrl)
        {
            _showBuff = null;
            _backBuff = null;
            _color = 0;
            _alpha = 1.0f;
            _zoom = 1.0f;
            _beRefresh = false;
            _beChanged = true;
            _lastZ = 0.0f;
            _dispObjs = new List<DisplayObject>();
            _ctrl = ctrl;
            _worldTransform = Transform.Identity;
            _dispEventTrigger = new EventTrigger(this);

            _timer = new Timer();
            _timer.Interval = 1;
            _sw = new Stopwatch();
            renderUpdateTime = 17;

            start();
        }

        public bool bestart { get; set; }

        public void start()
        {
            _addEventListeners();            
            _during = 0.0f;            
            _sw.Reset();
            _sw.Start();
            _timer.Start();
            _dispEventTrigger.bestart = true;
            bestart = true;
        }

        public void stop()
        {
            _timer.Stop();
            _sw.Stop();
            _dispEventTrigger.bestart = false;
            bestart = false;
        }

        public uint color
        {
            get
            {
                return _color;
            }
            set
            {
                if (value != _color)
                {
                    _color = value;
                    _beChanged = true;
                }
            }
        }

        public float alpha
        {
            get
            {
                return _alpha;
            }
            set
            {
                if (value != _alpha)
                {
                    _alpha = value;
                    _beChanged = true;
                }
            }
        }

        static internal Transform worldTransform
        {
            get
            {
                return _worldTransform;
            }
            set
            {
                if (value != _worldTransform)
                {                    
                    _worldTransform = value;
                }
            }
        }

        static public NGraphics.Point CursorPos
        {
            get
            {
                System.Drawing.Point p = _ctrl.PointToClient(Cursor.Position);
                return new NGraphics.Point(p.X, p.Y);
            }
        }

        public float zoom
        {
            get
            {
                return _zoom;
            }
            set
            {
                if (value != _zoom)
                {
                    _beChanged = true;
                    _zoom = value;
                    _worldTransform = Transform.Identity;
                    worldTransform *= Transform.Scale(_zoom);
                }
            }
        }

        public int width
        {
            get
            {
                return _ctrl.Width;
            }
            set
            {
                _ctrl.Width = value;
            }
        }

        public int height
        {
            get
            {
                return _ctrl.Height;
            }
            set
            {
                _ctrl.Height = value;
            }
        }

        public IEnumerable eachDisplayObject()
        {
            for (int i = 0; i < _dispObjs.Count; i++)
            {
                yield return _dispObjs[i];
            }
        }

        public int numDisplayObjects
        {
            get
            {
                return _dispObjs.Count;
            }
        }

        public void add(DisplayObject obj)
        {            
            if (_dispObjs.IndexOf(obj) < 0)
            {            
                if (obj.z == 0)
                    obj.z = ++_lastZ;
                _dispObjs.Add(obj);
                obj.stage = this;
                obj.addEventListener(EventType.Change, _OnChanged);
                obj.emit(EventType.AddToStage, null);
                _beChanged = true;
            }
        }

        public void remove(DisplayObject obj)
        {
            if ( _dispObjs.Remove(obj) )
            {
                obj.stage = null;
                obj.clearEventSettings();
                obj.removeEventListener(EventType.Change, _OnChanged);
                obj.emit(EventType.RemoveFromStage, null);
                _beChanged = true;
            }
        }

        private void _OnChanged(Event evt)
        {
            _beChanged = true;
        }

        private void _addEventListeners()
        {
            _ctrl.Load += _OnLoad;
            _ctrl.Paint += _OnPaint;
            _ctrl.Resize += _OnResize;
            _ctrl.MouseDown += _OnMouseDown;
            _ctrl.MouseUp += _OnMouseUp;
            _ctrl.MouseMove += _OnMouseMove;
            _ctrl.MouseHover += _OnMouseHover;
            _ctrl.MouseEnter += _OnMouseEnter;
            _ctrl.MouseLeave += _OnMouseLeave;
            _ctrl.KeyDown += _OnKeyDown;
            _ctrl.KeyUp += _OnKeyUp;
            _timer.Tick += _OnTimer;
        }

        private void _removeEventListeners()
        {
            _ctrl.Load -= _OnLoad;
            _ctrl.Paint -= _OnPaint;
            _ctrl.Resize -= _OnResize;
            _ctrl.MouseDown -= _OnMouseDown;
            _ctrl.MouseUp -= _OnMouseUp;
            _ctrl.MouseMove -= _OnMouseMove;
            _ctrl.MouseHover -= _OnMouseHover;
            _ctrl.MouseEnter -= _OnMouseEnter;
            _ctrl.MouseLeave -= _OnMouseLeave;
            _ctrl.KeyDown -= _OnKeyDown;
            _ctrl.KeyUp -= _OnKeyUp;
            _timer.Tick -= _OnTimer;
        }

        private void _OnLoad(object sender, EventArgs e)
        {
            _emit(EventType.StageOnLoaded, null);
        }

        private void _OnTimer(object sender_, EventArgs args)
        {
            if (_backBuff == null)
            {
                _DrawBackBuff(0);
                return;
            }

            bool beRender = false;
            _sw.Stop();
            float deltaTime = (float)_sw.Elapsed.TotalMilliseconds;
            _sw.Reset();
            _sw.Start();

            _during += deltaTime;
            while (_during >= renderUpdateTime)
            {
                _during -= renderUpdateTime;
                beRender = true;
            }

            if (_beRefresh)
            {
                _beRefresh = false;
                _ctrl.Refresh();
            }
            if (beRender)
            {
                _emit(EventType.StageOnUpdate, deltaTime);
                _emit(EventType.StageOnLastUpdate, deltaTime);
                _DrawBackBuff(deltaTime);
            }
        }

        private void _OnPaint(object sender, PaintEventArgs e)
        {
            _emit(EventType.StageOnPainted, null);
            _DrawShowBuff(e.Graphics);            
        }

        private void _DrawBackBuff(float deltaTime)
        {            
            if (_backBuff == null)
            {
                _backBuff = new Bitmap(_ctrl.DisplayRectangle.Width, _ctrl.DisplayRectangle.Height);
            }

            for (int i = 0; i < _dispObjs.Count;i++)            
                _dispObjs[i].beforeRender();
            
            if (_beChanged)
            {                
                Graphics dc = Graphics.FromImage((System.Drawing.Image)_backBuff);
                dc.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                _Render(dc, deltaTime);
                _beChanged = false;
                _beRefresh = true;
            }
        }

        private int _sortRule(DisplayObject a, DisplayObject b)
        {
            return a.z.CompareTo(b.z);
        }

        private void _Render(Graphics graphics, float deltaTime)
        {
            // emit changing events before render...
            GraphicsCanvasEx canvas = new GraphicsCanvasEx(graphics);
            canvas.Clear(Util.HexColorNG(color, alpha).GetColor());
            _dispObjs.Sort(_sortRule);
            for (int i = 0; i < _dispObjs.Count; i++)
            {
                if (_dispObjs[i].visible)                
                    _dispObjs[i].render(canvas, deltaTime);
                _dispObjs[i].afterRender();
            }
            canvas.SaveState();
        }

        private void _emit(EventType type, object data)
        {            
            if (EventDispatcher.CountEvents(type) > 0)
            {
                for (int i = 0; i < _dispObjs.Count; i++)
                {
                    if (_dispObjs[i].visible)
                        _dispObjs[i].emit(type, data);
                }
            }
            emit(type, data);
        }

        private void _OnResize(object sender, EventArgs e)
        {
            _emit(EventType.StageOnResized, e);
        }

        private void _OnMouseDown(object sender, MouseEventArgs e)
        {
            _emit(EventType.StageOnMouseDown, e);
        }

        private void _OnMouseUp(object sender, MouseEventArgs e)
        {
            _emit(EventType.StageOnMouseUp, e);
        }

        private void _OnMouseMove(object sender, MouseEventArgs e)
        {
            _emit(EventType.StageOnMouseMove, e);
        }

        private void _OnMouseHover(object sender, EventArgs e)
        {
            _emit(EventType.StageOnMouseHover, e);
        }

        private void _OnMouseEnter(object sender, EventArgs e)
        {
            _emit(EventType.StageOnMouseEnter, e);
        }

        private void _OnMouseLeave(object sender, EventArgs e)
        {
            _emit(EventType.StageOnMouseLeave, e);
        }

        private void _OnKeyDown(object sender, KeyEventArgs e)
        {
            _emit(EventType.StageOnKeyDown, e);
        }

        private void _OnKeyUp(object sender, KeyEventArgs e)
        {
            _emit(EventType.StageOnKeyUp, e);
        }

        private void _DrawShowBuff(Graphics graphx)
        {
            if (_backBuff == null)
            {
                return;
            }

            if (_showBuff != null)
            {
                _showBuff.Dispose();
            }
            _showBuff = (Bitmap)_backBuff.Clone();
            BitmapData data = _showBuff.LockBits(new System.Drawing.Rectangle(0, 0, _showBuff.Width, _showBuff.Height), ImageLockMode.ReadOnly, _showBuff.PixelFormat);
            _showBuff.UnlockBits(data);
            graphx.DrawImage(_showBuff, 0, 0);
        }

    }
}
