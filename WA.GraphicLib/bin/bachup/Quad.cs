﻿using NGraphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public class Quad : DisplayObject
    {
        private bool _beDraw;

        public Quad()
        {
            _beDraw = false;
        }

        public void draw(float _x, float _y, float _width, float _height)
        {
            _beDraw = true;
            _drawBoundary.left = _x;
            _drawBoundary.top = _y;
            _drawBoundary.right = _x + _width;
            _drawBoundary.bottom = _y + _height;
            emit(EventType.ReDraw, null);
        }

        internal override void render(GraphicsCanvasEx canvas, float deltaTime)
        {
            if (!_beDraw)
                return;

            // create pen & brush & transform....
            Brush fillBrush;
            Pen pen;
            setupBrushAndPen(out fillBrush, out pen);

            // scale*rotation*translation....
            applyTransform(canvas);

            if (beFillColor)
            {
                canvas.FillRectangle(0,0, _drawBoundary.dw, _drawBoundary.dh, fillBrush);
            }
            canvas.DrawRectangle(0,0, _drawBoundary.dw, _drawBoundary.dh, pen, brush);
        }
    }
}
