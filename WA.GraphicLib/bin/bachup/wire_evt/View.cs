﻿using NGraphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WA.GraphicLib;

namespace WAGraphicLibTestNodes
{
    public enum ViewActions
    {
        None = 0,
        StartLinking,
        NodeMoving,
        CheckLinked
    }

    public class View :EventDispatcher
    {        
        private Stage _stage;

        private List<Node> _nodes;        

        private Action<string, object[]> _logInfo;
        private Action<string, object[]> _logInfoAppend;

        private ViewActions _viewActions;
        private NGraphics.Point _linkingPt;
        private Node _linkingNd;
        private DrawPath _linkingPath;
        private NodeLinkingType _linkingType;
        private bool _bedrag;

        private Node _movingNd;

        private Wires _wires;
        private Wire _focusWire;
        private Node _focusNd;

        public View(Stage stage)
        {            
            _stage = stage;            
            _nodes = new List<Node>();            
            _viewActions = ViewActions.None;
            _bedrag = false;
            _linkingPt = NGraphics.Point.Zero;
            _linkingNd = null;
            _movingNd = null;
            _focusNd = null;
            _focusWire = null;
            _linkingPath = new DrawPath();
            _stage.add(_linkingPath);

            _linkingPath.border = 3.0f;
            _linkingPath.color = 0xFF7F00;

            _linkingType = NodeLinkingType.None;
            _wires = new Wires(stage);
            _wires.addEventListener(NodeEventType.OnNodeWireClick, _OnWireClick);
        }

        public void bindLogger(Action<string, object[]> loginfo,
                               Action<string, object[]> loginfoAppend)
        {
            _logInfo = loginfo;
            _logInfoAppend = loginfoAppend;
        }

        public void start()
        {
            bedrag = true;
            _stage.addEventListener(EventType.StageOnUpdate, _OnUpdate);
            _stage.addEventListener(EventType.StageOnMouseDown, _OnStageMouseDown);
            _stage.addEventListener(EventType.StageOnKeyDown, _OnStageKeyDown);
        }

        public void stop()
        {
            bedrag = false;
            _stage.removeEventListener(EventType.StageOnUpdate, _OnUpdate);
            _stage.removeEventListener(EventType.StageOnMouseDown, _OnStageMouseDown);
            _stage.removeEventListener(EventType.StageOnKeyDown, _OnStageKeyDown);            
        }

        private void _OnStageKeyDown(Event e)
        {
            KeyEventArgs arg = (KeyEventArgs)e.data;
            if (arg.KeyCode ==Keys.Delete ) // delete
            {
                deleteFocusItem();
            }
        }

        public void deleteFocusItem()
        {
            if (_focusWire != null)
            {
                _wires.remove(_focusWire);
                _stage.remove(_focusWire.disp);
                _focusWire = null;
            }
            if (_focusNd != null)
            {
                remove(_focusNd);
            }
        }

        private void _OnStageMouseDown(Event e)
        {
            MouseEventArgs arg = (MouseEventArgs)e.data;
            if ( !_wires.hasFocusAndClearFocus(new NGraphics.Point(arg.X, arg.Y)) )
            {
                _focusWire = null; // no more focus wire
            }
            if (null != _focusNd)
            {
                _focusNd.beFocus = false;
                _focusNd = null;
            }
        }

        public bool bedrag
        {
            get
            {
                return _bedrag;
            }
            set
            {
                _bedrag = value;
                for (int i = 0; i < _nodes.Count; i++)
                {
                    _nodes[i].disp.interactive.bedrag = _bedrag;
                }
            }
        }

        public void add(Node node)
        {
            if (!_nodes.Contains(node))
            {
                _nodes.Add(node);
                node.addEventListeners();
                node.disp.addEventListener(NodeEventType.OnNodeStartLinking, _OnNodeStartLinking);
                node.disp.addEventListener(NodeEventType.OnNodeEndLinking, _OnNodeEndLinking);
                node.disp.addEventListener(NodeEventType.OnNodeBeginMove, _OnNodeBeginMove);
                node.disp.addEventListener(NodeEventType.OnNodeEndMove, _OnNodeEndMove);
                node.disp.addEventListener(NodeEventType.OnNodeClick, _OnNodeClick);
                node.disp.scaleX =
                node.disp.scaleY = 0.7f;
                node.disp.interactive.bedrag = true;                
                _stage.add(node.disp);
            }
        }

        private void _OnNodeClick(Event e)
        {
            if (null != _focusNd)
                _focusNd.beFocus = false;
            EventNodeArg arg = (EventNodeArg)e.data;
            _focusNd = arg.node;
            _focusNd.beFocus = true;            
        }

        private void _OnNodeBeginMove(Event e)
        {
            EventNodeArg arg = (EventNodeArg)e.data;
            _movingNd = arg.node;
            _viewActions = ViewActions.NodeMoving;
        }

        private void _OnNodeEndMove(Event e)
        {
            EventNodeArg arg = (EventNodeArg)e.data;
            _movingNd = null;
            _viewActions = ViewActions.None;
        }

        private void _OnNodeStartLinking(Event e)
        {
            bedrag = false;
            EventNodeArg arg = (EventNodeArg)e.data;
            _linkingPt = arg.pos;
            _linkingNd = arg.node;
            _linkingType = arg.type;
            _viewActions = ViewActions.StartLinking;
        }

        private void _OnNodeEndLinking(Event e)
        {
            bedrag = true;
            _viewActions = ViewActions.CheckLinked;
        }

        public void linkNodes(Node parent, Node child)
        {
            Wire wire = _wires.link(parent, child);
            if (null != wire)
                _stage.add(wire.disp);            
        }

        public void remove(Node node)
        {
            if (_nodes.Remove(node))
            {                
                node.removeEventListeners();
                node.disp.removeEventListener(NodeEventType.OnNodeStartLinking, _OnNodeStartLinking);
                node.disp.removeEventListener(NodeEventType.OnNodeEndLinking, _OnNodeEndLinking);
                node.disp.removeEventListener(NodeEventType.OnNodeBeginMove, _OnNodeBeginMove);
                node.disp.removeEventListener(NodeEventType.OnNodeEndMove, _OnNodeEndMove);
                node.disp.removeEventListener(NodeEventType.OnNodeClick, _OnNodeClick);
                _stage.remove(node.disp);
                Wire[] ws = _wires.find(node);
                for (int i = 0; i < ws.Length; i++)
                {
                    var wire = ws[i];
                    _wires.remove(wire);
                    _stage.remove(wire.disp);
                }
            }
        }

        private bool _beHitOtherNode(Node orig, NGraphics.Point pt, out Node hitNode)
        {
            hitNode = null;
            for (int i = 0; i < _nodes.Count; i++)
            {
                Node node = _nodes[i];
                if (node.guid != orig.guid &&
                    node.disp.hitTestPoint((float)pt.X, (float)pt.Y))
                {
                    hitNode = node;
                    return true;
                }
            }
            return false;
        }

        private void _OnUpdate(Event e)
        {
            if (_viewActions == ViewActions.StartLinking)
            {
                if (_linkingNd == null)
                {
                    bedrag = true;                    
                    _linkingNd = null;
                    _viewActions = ViewActions.None;
                    return;
                }

                NGraphics.Point st_pt = _linkingPt;
                NGraphics.Point ed_pt = Stage.CursorPos;

                float nw = (int)_linkingNd.disp.width,
                      nh = 3;
                _linkingPath.visible = true;
                Wire.drawLink(_linkingPath, st_pt, ed_pt, nw, nh,
                    _linkingType == NodeLinkingType.OutputToInput ? 1:-1);
            }
            else if (_viewActions == ViewActions.CheckLinked)
            {
                NGraphics.Point ed_pt = Stage.CursorPos;
                for (int i = 0; i < _nodes.Count; i++)
                {
                    if (_linkingType == NodeLinkingType.OutputToInput)
                    {
                        if (_nodes[i].beHitInputGlue(ed_pt))
                        {
                            Wire wire = _wires.link(_linkingNd, _nodes[i]);
                            if (null != wire)
                                _stage.add(wire.disp);
                            break;
                        }
                        Node hitnd;
                        if (_beHitOtherNode(_nodes[i], ed_pt, out hitnd))
                        {
                            Wire wire = _wires.link(_linkingNd, hitnd);
                            if (null != wire)
                                _stage.add(wire.disp);
                            break;
                        }
                    } else if (_linkingType == NodeLinkingType.InputToOutput)
                    {
                        if (_nodes[i].beHitOutputGlue(ed_pt))
                        {
                            Wire wire = _wires.link(_nodes[i], _linkingNd);
                            if (null != wire)
                                _stage.add(wire.disp);
                            break;
                        }
                    }
                }
                _linkingPath.erase();
                _linkingPath.visible = false;
                _linkingNd = null;
                _linkingType = NodeLinkingType.None;
                _viewActions = ViewActions.None;
            }
            else if (_viewActions == ViewActions.NodeMoving &&
                     _movingNd != null)
            {
                Wire[] wires = _wires.find(_movingNd);
                for (int i = 0; i < wires.Length; i++ )
                {
                    wires[i].redraw();
                }
            }
            else
            {

            }
        }

        private void _OnWireClick(Event evt)
        {
            EventNodeArg arg = (EventNodeArg)evt.data;
            _focusWire = arg.wire;
        }

        private void _Loginfo(string format, params object[] args)
        {
            if (_logInfo!= null)
                _logInfo(format, args);
        }

        private void _LoginfoAppend(string format, params object[] args)
        {
            if (_logInfoAppend!= null)
                _logInfoAppend(format, args);
        }
    }
}
