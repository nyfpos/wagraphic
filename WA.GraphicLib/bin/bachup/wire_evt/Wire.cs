﻿using NGraphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WA.GraphicLib;

namespace WAGraphicLibTestNodes
{
    public class Wire :EventDispatcher
    {
        private const float MinRange = 6;        

        public Node node1
        {
            get;
            private set;
        }
        public Node node2
        {
            get;
            private set;
        }
        static public float lastWireZ = -100;

        private DrawPath _link;
        private bool _befocus;

        public Wire(Node n1, Node n2)
        {
            node1 = n1;
            node2 = n2;
            _befocus = false;
            _link = new DrawPathEx();            
            _link.z = lastWireZ;
            _link.border = 3.0f;
            _link.color = 0x808080;         
            redraw();
        }

        public DisplayObject disp
        {
            get
            {
                return _link;
            }
        }
       
        public bool befocus
        {
            get
            {
                return _befocus;
            }
            set
            {
                _befocus = value;
                if (_befocus)
                {
                    --_link.z;
                    _link.color = 0xFF7F00;
                }
                else
                {
                    _link.z = lastWireZ;
                    _link.color = 0x808080;
                }
            }
        }

        public bool hitTestPoint(NGraphics.Point pt)
        {
            bool ret = false;
            Point min, max;
            List<Boundary> listRect = new List<Boundary>();
            NGraphics.Point prev = new Point(double.MinValue, double.MinValue);
            foreach (NGraphics.Point orgpt in _link.eachBoundaryPoint())
            {
                if (prev.X != double.MinValue ||
                    prev.Y != double.MinValue)
                {
                    Util.GetMinMaxPoint(new Point[] { prev, orgpt }, out min, out max);
                    Boundary b = new Boundary();
                    b.left = (float)min.X;
                    b.top = (float)min.Y;
                    b.right = (float)max.X;
                    b.bottom = (float)max.Y;
                    listRect.Add(b);
                }

                prev = orgpt;
            }

#if ShowCurveTestRange            
            for (int i = 0; i < listRect.Count; i++)
            {
                Boundary boundary = listRect[i];

                float centerx = boundary.left + boundary.dw * 0.5f;
                float centery = boundary.top + boundary.dh * 0.5f;
                float r = Math.Max(boundary.dw, boundary.dh)*0.5f + MinRange;

                // for test
                Stage stage = Form1.getStage;
                var q = new WA.GraphicLib.Ellipse();
                q.z = -99999;
                q.draw(centerx-r, centery-r, 2*r,2*r);
                stage.add(q);
            }
#endif

            for (int i = 0; i < listRect.Count; i++ )
            {
                Boundary boundary = listRect[i];

                // exchange rect hit test for circle hit test...
                /*
                if(!(pt.X > boundary.right ||
                     pt.X < boundary.left ||
                     pt.Y > boundary.bottom ||
                     pt.Y < boundary.top))
                {
                    ret = true;
                    break;
                }
                 */
                float centerx = boundary.left + boundary.dw * 0.5f;
                float centery = boundary.top + boundary.dh * 0.5f;
                float r = Math.Max(boundary.dw, boundary.dh)*0.5f + MinRange;
                float dx = (float)pt.X - centerx;
                float dy = (float)pt.Y - centery;
                float dist = dx * dx + dy * dy;
                if (dist <= r*r)
                {
                    ret = true;
                    break;
                }
            }
            return ret;
        }

        public void redraw()
        {
            NGraphics.Point st_pt = node1.glueOutputPos;
            NGraphics.Point ed_pt = node2.glueInputPos;

            float nw = (int)node1.disp.width,
                  nh = 3;

            Wire.drawLink(_link, st_pt, ed_pt, nw, nh);
        }

        static public void drawLink(DrawPath path, NGraphics.Point st_pt,
                                    NGraphics.Point ed_pt, float nw, float nh,
                                    int dir = 1)
        {
            float dx = (float)(ed_pt.X - st_pt.X);
            float dy = (float)(ed_pt.Y - st_pt.Y);
            float delta = (float)Math.Sqrt(dy * dy + dx * dx);
            float scale = 1;
            if (delta < nw)
            {
                scale = 0.75f - 0.75f * ((nw - delta) / nw);
            }
            path.draw(new List<PathOp>()
                    {
                        new MoveTo(st_pt),
                        new CurveTo(new NGraphics.Point(st_pt.X+dir*scale*nw, st_pt.Y+dir*nh),
                                    new NGraphics.Point(ed_pt.X-dir*scale*nw, ed_pt.Y-dir*nh),
                                    ed_pt)
                    });            
        }
    }

    public class Wires : EventDispatcher
    {
        private Stage _stage;
        private List<Wire> _tmpwires;
        private List<Wire> _wires;
        private Dictionary<string, Wire> _nodesWire;
        private Wire _curFocusWire;
        private List<DisplayObject> _tmpDisps;

        public Wires(Stage stage)
        {
            _stage = stage;
            _wires = new List<Wire>();
            _tmpwires = new List<Wire>();
            _nodesWire = new Dictionary<string, Wire>();
            _tmpDisps = new List<DisplayObject>();
            _curFocusWire = null;
            _stage.addEventListener(EventType.StageOnMouseDown, _OnMouseDownStage);
        }

        public int Count
        {
            get
            {
                return _wires.Count;
            }
        }

        public IEnumerable each()
        {
            for (int i = 0; i < _wires.Count; i++)
            {
                yield return _wires[i];
            }
        }

        public Wire[] find(Node node)
        {
            _tmpwires.Clear();
            foreach (Wire wire in each())
            {
                if (wire.node1.guid == node.guid ||
                    wire.node2.guid == node.guid)
                {
                    _tmpwires.Add(wire);
                }
            }
            return _tmpwires.ToArray();
        }

        public Wire link(Node node1, Node node2)
        {
            if (node1 == null || node2 == null)
                return null;

            string key = node1.guid + node2.guid;

            if (!_nodesWire.ContainsKey(key))
            {
                Wire wire = new Wire(node1, node2);
                wire.disp.data = wire;
                //wire.disp.addEventListener(EventType.MouseDown, _OnMouseDownWire, wire);
                _wires.Add(wire);
                _nodesWire.Add(key, wire);
                return wire;
            }
            return null;
        }

        public void remove(Wire wire)
        {
            string key = wire.node1.guid + wire.node2.guid;
            if (_wires.Remove(wire))
            {
                //wire.disp.removeEventListener(EventType.MouseDown, _OnMouseDownWire);
            }
            _nodesWire.Remove(key);
        }

        private void _OnMouseDownStage(Event evt)
        {
            _tmpDisps.Clear();
            bool needFindWire = true;
            MouseEventArgs e = (MouseEventArgs)evt.data;            
            foreach (DisplayObject disp in _stage.eachDisplayObject())
            {
                if ( disp.hitTestPoint(e.X, e.Y))
                {
                    if (disp is DrawPathEx &&
                        disp.data is Wire)
                    {
                        _tmpDisps.Add(disp);
                    }
                    else
                    {
                        needFindWire = false;
                        break;
                    }
                }                
            }
            if (needFindWire &&
                _tmpDisps.Count > 0)
            {
                DisplayObject maxZDisp = null;
                for (int i = 0; i < _tmpDisps.Count; i++)
                {
                    if (null == maxZDisp)
                    {
                        maxZDisp = _tmpDisps[i];
                    }
                    else if (_tmpDisps[i].z >= maxZDisp.z)
                    {
                        maxZDisp = _tmpDisps[i];
                    }
                }
                if (null != maxZDisp)
                {
                    _OnMouseDownWire(new Event(evt.eventType, e, maxZDisp.data));
                }
            }
        }

        private void _OnMouseDownWire(Event evt)
        {
            MouseEventArgs evtargs = (MouseEventArgs)evt.data;
            Point pos = new Point(evtargs.X, evtargs.Y);
            Wire wire = evt.arg as Wire;
            if (wire!= null && wire.hitTestPoint(pos))
            {
                if (_curFocusWire != null)
                    _curFocusWire.befocus = false;
                
                EventNodeArg arg = new EventNodeArg();
                arg.wire = wire;
                emit(NodeEventType.OnNodeWireClick, arg);
                wire.befocus = true;
                _curFocusWire = wire;
            }
        }

        public bool hasFocusAndClearFocus(NGraphics.Point pt)
        {
            int remain = _wires.Count;
            for (int i = 0; i < _wires.Count; i++)
            {
                if (_wires[i].befocus )
                {
                    if (!_wires[i].disp.hitTestPoint(pt) ||
                        !_wires[i].hitTestPoint(pt))
                    {
                        _wires[i].befocus = false;
                    }
                    else
                    {
                        continue;
                    }
                }
                --remain;
            }
            if (remain <= 0)
                _curFocusWire = null;

            return remain > 0 ;
        }
        
    };
}
