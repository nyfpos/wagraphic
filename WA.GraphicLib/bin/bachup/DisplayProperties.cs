﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{    
    public class GlobalTransform
    {
        private DisplayObjectContainer _disp;

        // check if there's parent transform....
        public GlobalTransform(DisplayObjectContainer disp)
        {
            _disp = disp;
        }
        
        public NGraphics.Transform toMatrix()
        {
            NGraphics.Transform transform = NGraphics.Transform.Translate(_disp.x, _disp.y) *
                                            NGraphics.Transform.Rotate(_disp.rotateZ) *
                                            NGraphics.Transform.Scale(_disp.scaleX, _disp.scaleY);

            if (_disp.parent != null)
            {
                transform = _disp.parent.global.toMatrix() * transform;
            }

            return transform;
        }
    }

    internal class LocalTransform
    {
        private DisplayObject _disp;

        public float x { get; set; }
        
        public float y { get; set; }
        
        public float z { get; set; }
        
        public float rotateZ 
        { 
            get
            {
                return _rotateZ;
            } 
            set
            {
                _rotateZ = value;
                while (_rotateZ > 360)
                    _rotateZ -= 360;
            }        
        }
        
        public float scaleX { get; set; }
        
        public float scaleY { get; set; }

        private float _rotateZ;

        // local transform for every DisplayObjects...
        public LocalTransform(DisplayObject disp)
        {
            x = y = z = 0;
            _rotateZ = 0;
            scaleX = scaleY = 1;            
            _disp = disp;
        }

        public NGraphics.Transform toMatrix()
        {
            NGraphics.Transform transform = NGraphics.Transform.Translate(_disp.x, _disp.y) *
                                            NGraphics.Transform.Rotate(rotateZ) *
                                            NGraphics.Transform.Scale(scaleX, scaleY)/* *
                                            NGraphics.Transform.Translate(-_disp.x, -_disp.y) *
                                            NGraphics.Transform.Translate(x, y);*/;
            return transform;            
        }
    }

    //    p0------p1
    //    |       |
    //    |       |
    //    p2------p3  
    public struct Boundary
    {
        public float left;
        public float top;
        public float right;
        public float bottom;
        public float dw
        {
            get
            {
                float w = (float)(right - left);
                return w <= 0 ? 1 : w;
            }
        }
        public float dh
        {
            get
            {
                float h = (float)(bottom - top);
                return h <= 0 ? 1 : h;
            }
        }

        public NGraphics.Point p0 
        {
            get
            {
                return new NGraphics.Point(left, top);
            }
            set
            {
                left = (float)value.X;
                top = (float)value.Y;
            }
        }
        public NGraphics.Point p1 
        {
            get
            {
                return new NGraphics.Point(right, top);
            }
            set
            {
                right = (float)value.X;
                top = (float)value.Y;
            }
        }
        public NGraphics.Point p2
        {
            get
            {
                return new NGraphics.Point(left, bottom);
            }
            set
            {
                left = (float)value.X;
                bottom = (float)value.Y;
            }
        }
        public NGraphics.Point p3 
        { 
            get
            {
                return new NGraphics.Point(right, bottom);
            }
            set
            {
                right = (float)value.X;
                bottom = (float)value.Y;
            }
        }
    }

}
