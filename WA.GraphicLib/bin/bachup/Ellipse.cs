﻿using NGraphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public class Ellipse :DisplayObject
    {
        private bool _beDraw;

        public Ellipse()
        {
            _beDraw = false;
        }

        public void draw(float _x, float _y, float _width, float _height)
        {
            _beDraw = true;
            _drawBoundary.left = _x;
            _drawBoundary.top = _y;
            _drawBoundary.right = _x + _width;
            _drawBoundary.bottom = _y + _height;
            emit(EventType.ReDraw, null);
        }

        internal override void render(GraphicsCanvasEx canvas, float deltaTime)
        {
            if (!_beDraw)
                return;

            // create pen & brush & transform....
            Brush fillBrush;
            Pen pen;
            setupBrushAndPen(out fillBrush, out pen);

            // scale*rotation*translation....
            applyTransform(canvas);

            if (beFillColor)
            {
                canvas.FillEllipse(0,0, _drawBoundary.dw, _drawBoundary.dh, fillBrush);
            }
            canvas.DrawEllipse(0,0, _drawBoundary.dw, _drawBoundary.dh, pen, brush);
        }
    }
}
