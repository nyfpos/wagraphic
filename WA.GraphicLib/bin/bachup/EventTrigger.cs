﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WA.GraphicLib
{
    internal class EventTrigger
    {
        private List<DisplayObject> _tmpdisp;
        private Stage _stage;
        private bool _beFocusStage;

        private bool _bestart;
        public bool bestart
        {
            get
            {
                return _bestart;
            }

            set
            {
                _bestart = value;
                if (_bestart)
                    _addEventListeners();
                else
                    _removeEventListeners();
            }
        }

        public EventTrigger (Stage stage)
        {
            _beFocusStage = false;
            _stage = stage;
            _tmpdisp = new List<DisplayObject>();
        }

        public void _addEventListeners()
        {
            _stage.addEventListener(EventType.StageOnMouseEnter, _OnStageMouseEnter);
            _stage.addEventListener(EventType.StageOnMouseLeave, _OnStageMouseLeave);
            _stage.addEventListener(EventType.StageOnMouseDown, _OnMouseDown);
            _stage.addEventListener(EventType.StageOnMouseUp, _OnMouseUp);
            _stage.addEventListener(EventType.StageOnUpdate, _OnUpdate);
        }

        public void _removeEventListeners()
        {
            _stage.removeEventListener(EventType.StageOnMouseEnter, _OnStageMouseEnter);
            _stage.removeEventListener(EventType.StageOnMouseLeave, _OnStageMouseLeave);
            _stage.removeEventListener(EventType.StageOnMouseDown, _OnMouseDown);
            _stage.removeEventListener(EventType.StageOnMouseUp, _OnMouseUp);
            _stage.removeEventListener(EventType.StageOnUpdate, _OnUpdate);
        }

        private void _OnStageMouseEnter(Event evt)
        {
            _beFocusStage = true;
        }

        private void _OnStageMouseLeave(Event evt)
        {
            _beFocusStage = false;
        }

        private void _OnUpdate(Event evt)
        {
            if (!_beFocusStage)
                return;
            if (EventDispatcher.CountEvents(EventType.MouseEnter) > 0 ||
                EventDispatcher.CountEvents(EventType.MouseLeave) > 0)
            {
                float deltaTime = (float)evt.data;
                deltaTime /= 1000.0f;
                
                List<DisplayObject> hoveredObjs = _findObjectsAtPos();
                //hoveredObjs.Sort(_sortRule);
                // _tmpdisp at the current cursor point...                
                DisplayObject maxDispObj = _findMaxZObj(hoveredObjs);
                if (maxDispObj!= null)
                {
                    if (!maxDispObj.hovered)
                    {
                        maxDispObj.hovered = true;
                        maxDispObj.emit(EventType.MouseEnter, Stage.CursorPos);
                    }
                    else if (maxDispObj.hovered &&
                             maxDispObj.mousedown)
                    {
                        maxDispObj.emit(EventType.MouseMove, Stage.CursorPos);
                    }
                }

                foreach (DisplayObject disp in _stage.eachDisplayObject())
                {
                    // check if object has been left
                    if (disp.hovered)
                    {                      
                        int index = hoveredObjs.IndexOf(disp);
                        if (index < 0 || (maxDispObj!=null && disp.guid != maxDispObj.guid))
                        {
                            disp.hovered = false;
                            disp.emit(EventType.MouseLeave, index < 0 ? MouseLeaveReason.LostHover : MouseLeaveReason.UnderOtherObjects);                       
                        }
                    }
                }
            }
        }

        private List<DisplayObject> _findObjectsAtPos(float ptx=float.MinValue, float pty=float.MinValue)
        {
            NGraphics.Point cursor = NGraphics.Point.Zero;//

            if (ptx == float.MinValue ||
                pty == float.MinValue)
            {
                cursor = Stage.CursorPos;
            }
            else
            {
                cursor.X = ptx;
                cursor.Y = pty;
            }

            _tmpdisp.Clear();
            foreach (DisplayObject disp in _stage.eachDisplayObject())
            {
                if (!disp.visible)
                    continue;
                if (disp.hitTestPoint((float)cursor.X, (float)cursor.Y))
                {
                    _tmpdisp.Add(disp); // objects have been hovered...
                }
            }
            return _tmpdisp;
        }

        private DisplayObject _findMaxZObj(List<DisplayObject> coll)
        {
            DisplayObject disp = null;
            for (int i = 0; i < coll.Count; i++)
            {
                if (disp == null)
                {
                    disp = coll[i];
                } else if (coll[i].z > disp.z)
                {
                    disp = coll[i];
                }
            }
            return disp;
        }

        private int _sortRule(DisplayObject a, DisplayObject b)
        {
            return -1 * a.z.CompareTo(b.z);
        }

        private void _OnMouseUp(Event evt)
        {
            if (EventDispatcher.CountEvents(EventType.MouseUp) > 0)
            {
                MouseEventArgs e = (MouseEventArgs)evt.data;
                //DisplayObject obj = _findMaxZObj(_findObjectsAtPos(e.X, e.Y));
                List<DisplayObject> objs = _findObjectsAtPos(e.X, e.Y);
                objs.Sort(_sortRule);
                for (int i = 0; i < objs.Count; i++)
                {
                    DisplayObject obj = objs[i];                    
                    obj.emit(EventType.MouseUp, Stage.CursorPos);
                    if (obj.mousedown)
                        obj.emit(EventType.MouseClick, e);
                    obj.mousedown = false;
                    if (obj.isPropagationStopped)
                        break;
                }
            }
        }

        private void _OnMouseDown(Event evt)
        {
            if (EventDispatcher.CountEvents(EventType.MouseDown) > 0)
            {                
                MouseEventArgs e = (MouseEventArgs)evt.data;
               // DisplayObject obj = _findMaxZObj(_findObjectsAtPos(e.X, e.Y));
                List<DisplayObject> objs = _findObjectsAtPos(e.X, e.Y);
                objs.Sort(_sortRule);
                for (int i = 0; i < objs.Count; i++)
                {
                    DisplayObject obj = objs[i];
                    if (obj != null)
                    {
                        obj.mousedown = true;
                        obj.emit(EventType.MouseDown, e);
                    }
                    if (obj.isPropagationStopped)
                        break;
                }
            }
        }

    }
}
