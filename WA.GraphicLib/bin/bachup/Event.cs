﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public enum MouseLeaveReason
    {
        UnderOtherObjects,
        LostHover
    }

    public class Event
    {
        public string eventType { get; private set; }

        public object sender { get; internal set; }

        public object target { get; internal set; }

        public object data { get; internal set; }

        public object arg { get; internal set; }

        public bool isPropagationStopped { get { return !_beBubble; } }

        public void stopPropagation() { _beBubble = false; }

        private bool _beBubble;

        internal Action<Event> callback;

        public Event(string type, bool beBubble=false)
        {
            eventType = type;
            _beBubble = beBubble;
        }
    }
}
