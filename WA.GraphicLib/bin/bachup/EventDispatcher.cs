﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public class EventDispatcher : IDisposable
    {
        static readonly string EventTypePrefix = "wa.graphiclib.eventtype_";

        static private Dictionary<string, int> _CountEvents;
        static EventDispatcher()
        {
            _CountEvents = new Dictionary<string, int>();
        }

        static public int CountEvents(EventType type)
        {
            return CountEvents(EventTypePrefix+ type.ToString());
        }

        static public int CountEvents(string type)
        {
            int count = 0;
            if (_CountEvents.TryGetValue(type, out count))
            {
                return count;
            }
            return 0;
        }

        private Dictionary<string, HashSet<Action<Event>>> _existCallbacks;
        private Dictionary<string, List<Event>> _events;

        private bool _bePropagationStopped;

        public EventDispatcher()
        {
            _bePropagationStopped = false;
            _events = new Dictionary<string, List<Event>>();
            _existCallbacks = new Dictionary<string, HashSet<Action<Event>>>();
        }

        public bool addEventListener(EventType type, Action<Event> callback, object arg = null, bool beBubble = false)
        {
            return addEventListener(EventTypePrefix + type.ToString(), callback, arg, beBubble);
        }

        public void removeEventListener(EventType type, Action<Event> callback)
        {
            removeEventListener(EventTypePrefix + type.ToString(), callback);
        }

        public bool emit(EventType type, object data)
        {
            return emit(EventTypePrefix + type.ToString(), data);
        }

        public bool addEventListener(string type, Action<Event> callback, object arg = null, bool beBubble=false)
        {
            if (null == callback)
                return false;

            HashSet<Action<Event>> setCallbacks;
            if (_existCallbacks.TryGetValue(type, out setCallbacks))
            {
                if (setCallbacks.Contains(callback))
                    return false;
            }
            else
            {
                setCallbacks = new HashSet<Action<Event>>();
                _existCallbacks.Add(type, setCallbacks);
            }
            var evt = new Event(type, beBubble);            
            evt.target = this;
            evt.arg = arg;
            evt.callback = callback;

            List<Event> set;
            if (!_events.TryGetValue(type, out set))
            {
                set = new List<Event>();
                _events.Add(type, set);
            }
            set.Add(evt);
            setCallbacks.Add(callback);

            if (_CountEvents.ContainsKey(type))
            {
                _CountEvents[type]++;
            }
            else
            {
                _CountEvents[type] = 1;
            }
            return true;
        }

        public bool isPropagationStopped { get { return _bePropagationStopped; } }

        public bool emit(string type, object data)
        {
            List<Event> evts;
            if (!_events.TryGetValue(type, out evts))
                return false;
            _bePropagationStopped = false;
            for (int i = 0; i < evts.Count; i++)
            {
                Event evt = evts[i];
                evt.data = data;
                if (evt.callback != null)
                {
                    try
                    {
                        evt.callback(evt);
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.Error(ex.Message);
                        Logger.Instance.Debug(ex.StackTrace);
                        return false;
                    }
                }
            }
            return true;
        }

        public void removeEventListener(string type, Action<Event> callback)
        {
            if (callback == null)
                return;
            List<Event> evts = null;
            if (_events.TryGetValue(type, out evts))
            {
                for (int i = 0; i < evts.Count; i++)
                {
                    if (evts[i].callback == callback)
                    {
                        evts.RemoveAt(i);
                        HashSet<Action<Event>> setCallbacks;
                        if (_existCallbacks.TryGetValue(type, out setCallbacks))
                        {
                            setCallbacks.Remove(callback);
                        }
                        if (_CountEvents.ContainsKey(type))
                        {
                            _CountEvents[type]--;
                        }
                        break;
                    }                    
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                foreach (var pair in _events)
                {
                    string type = pair.Key;
                    List<Event> evts = pair.Value;
                    if (_CountEvents.ContainsKey(type))
                    {
                        _CountEvents[type] -= evts.Count;
                    }
                    evts.Clear();
                }
                _events.Clear();
            }
        }
    }
}
