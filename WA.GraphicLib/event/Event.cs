﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public enum MouseLeaveReason
    {
        UnderOtherObjects,
        LostHover
    }

    public class Event
    {
        public string eventType { get; private set; }

        public object sender { get; internal set; }

        public object target { get; internal set; }

        public object data { get; internal set; }

        public object arg { get; internal set; }

        public object allTriggers { get; internal set; }

        internal Action<Event> callback;

        public Event(string type)
        {
            eventType = type;     
        }

        public Event(string type, object data_, object arg_)
        {
            eventType = type;
            data = data_;
            arg = arg_;
        }
    }
}
