﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public enum EventType
    {
        Paint = 0,
        
        StageOnLoaded,
        StageOnPainted,
        StageOnResized,
        StageOnMouseDown,
        StageOnMouseUp,
        StageOnMouseMove,
        StageOnMouseHover,
        StageOnMouseEnter,
        StageOnMouseLeave,
        StageOnKeyDown,
        StageOnKeyUp,
        StageOnUpdate,
        StageOnLastUpdate,

        AddToStage,
        RemoveFromStage,
        Change,
        ReDraw,
        MouseDown,
        MouseUp,
        MouseClick,
        MouseEnter,
        MouseMove,
        MouseLeave,

        SelectBegin,
        SelectEnd,
        SelectMoveBegin,
        SelectMoveEnd,
        SelectMoving,
        SelectScaling,
    }
}
