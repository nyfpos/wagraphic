﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public class Singleton<T> where T : class, new()
    {
        static private T _instance;

        static Singleton()
        {
            _instance = null;
        }

        static public T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new T();
                }
                return _instance;
            }
        }
    }
}
