﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WA.GraphicLib
{
    public enum ImageCompareResult
    {
        ciCompareOk,
        ciPixelMismatch,
        ciSizeMismatch
    };

    public class Util
    {
        /*
         * (lerp0) A
         * (lerp1) B
         * (lerp2) C
         * (lerp3) D
         * (lerp4) E
         * (lerp5) P
              p0--------A------cp0
                         \     |
                          \    |
                           D   |
                           |\  |
                           | \ |         
                           P   B
                           | / |        
                           |/  |
                           E   |
                          /    |
                         /     |    
              p1--------C------cp1                              
            A : p0 -> cp0
            B : cp0 -> cp1
            C : cp1 -> p1
         */
        static public NGraphics.Point ApproximateBezier(NGraphics.Point p0, NGraphics.Point cp0,
                                             NGraphics.Point cp1, NGraphics.Point p1, float t)
        {
            NGraphics.Point A, B, C, D, E;
            A = lerp_pt(p0, cp0, t);
            B = lerp_pt(cp0, cp1, t);
            C = lerp_pt(cp1, p1, t);
            D = lerp_pt(A, B, t);
            E = lerp_pt(B, C, t);
            return lerp_pt(D, E, t);            
        }

        static public NGraphics.Point lerp_pt(NGraphics.Point p1, NGraphics.Point p2, double t)
        {
            return new NGraphics.Point(lerp(p1.X, p2.X, t), lerp(p1.Y, p2.Y, t));
        }

        static public double lerp(double start, double end, double t)
        {
            return (1 - t) * start + t * end;
        }

        static public float lerp_f(float start, float end, float t)
        {
            return (float)lerp(start, end, t);
        }

        static public void GetMinMaxPoint(NGraphics.Point[] points, out NGraphics.Point minXY, out NGraphics.Point maxXY)
        {
            minXY = new NGraphics.Point(float.MaxValue, float.MaxValue);
            maxXY = new NGraphics.Point(float.MinValue, float.MinValue);            
            for (int i = 0; i < points.Length; i++)
            {                
                double x = points[i].X;
                double y = points[i].Y;                    
                if (x < minXY.X)
                    minXY.X = x;
                if (y < minXY.Y)
                    minXY.Y = y;
                if (x > maxXY.X)
                    maxXY.X = x;
                if (y > maxXY.Y)
                    maxXY.Y = y;                
            }
        }

        static public NGraphics.Brush HexColorToBrushNG(uint color, float alpha)
        {
            return new NGraphics.SolidBrush(HexColorNG(color, alpha));
        }

        static public NGraphics.Color HexColorNG(uint color, float alpha)
        {
            uint tmp = 0;
            tmp = color & 0x00FF0000;
            byte r = (byte)(tmp >> 16);
            tmp = color & 0x0000FF00;
            byte g = (byte)(tmp >> 8);
            tmp = color & 0x000000FF;
            byte b = (byte)tmp;
            return new NGraphics.Color(r / 255.0, g / 255.0, b / 255.0, alpha);
        }

        static public ImageCompareResult Compare(Bitmap bmp1, Bitmap bmp2)
        {
            ImageCompareResult cr = ImageCompareResult.ciCompareOk;

            //Test to see if we have the same size of image
            if (bmp1.Size != bmp2.Size)
            {
                cr = ImageCompareResult.ciSizeMismatch;
            }
            else
            {
                //Convert each image to a byte array
                System.Drawing.ImageConverter ic =
                       new System.Drawing.ImageConverter();
                byte[] btImage1 = new byte[1];
                btImage1 = (byte[])ic.ConvertTo(bmp1, btImage1.GetType());
                byte[] btImage2 = new byte[1];
                btImage2 = (byte[])ic.ConvertTo(bmp2, btImage2.GetType());

                //Compute a hash for each image
                SHA256Managed shaM = new SHA256Managed();
                byte[] hash1 = shaM.ComputeHash(btImage1);
                byte[] hash2 = shaM.ComputeHash(btImage2);

                //Compare the hash values
                for (int i = 0; i < hash1.Length && i < hash2.Length
                                  && cr == ImageCompareResult.ciCompareOk; i++)
                {
                    if (hash1[i] != hash2[i])
                        cr = ImageCompareResult.ciPixelMismatch;
                }
            }
            return cr;
        }
    }
}
