﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NGraphics;
using WA.GraphicLib;

namespace WAGraphicLibTestCar
{
    public class Body
    {
        private Quad _quad;
        private Sprite _sprite;
        private Line[] _lines;

        public Body()
        {
            _sprite = new Sprite();
            _quad = new Quad();
            _lines = new Line[]
            {
                new Line(),
                new Line(),
                new Line()
            };

            _sprite.addChild(_lines[0]);
            _sprite.addChild(_lines[1]);
            _sprite.addChild(_lines[2]);
            _sprite.addChild(_quad);

            _lines[0].draw(new Point(66, 0), new Point(86, 20));
            _lines[1].draw(new Point(86, 20), new Point(86, 40));
            _lines[2].draw(new Point(86, 40), new Point(66, 40));
            _quad.draw(0, 0, 66, 40);
        }

        public Sprite disp
        {
            get
            {
                return _sprite;
            }
        }
    }
}
