﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NGraphics;
using WA.GraphicLib;

namespace WAGraphicLibTestCar
{
    public class Wheel
    {        
        private Sprite _sprite;
        private WA.GraphicLib.Ellipse _wheel;
        private Line[] _lines;

        public Wheel()
        {
            _sprite = new Sprite();            
            _wheel = new WA.GraphicLib.Ellipse();
            _wheel.draw(0, 0, 30, 30);
            _lines = new Line[]
            {
                new Line(),
                new Line(),
                new Line()
            };
            _lines[0].draw(new Point(15, 0), new Point(15, 15));
            _lines[1].draw(new Point(15, 15), new Point(30, 15));
            _lines[2].draw(new Point(15, 15), new Point(30, 15));
            _lines[1].rotateZ = 45;
            _lines[2].rotateZ = 135;

            _sprite.addChild(_wheel);
            for (int i = 0; i < _lines.Length; i++)
            {
                _sprite.addChild(_lines[i]);
            }
        }

        public Sprite disp
        {
            get
            {
                return _sprite;
            }
        }
    }
}
