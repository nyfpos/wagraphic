﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NGraphics;
using WA.GraphicLib;

namespace WAGraphicLibTestCar
{
    public class Car
    {
        private Sprite _sprite;
        private Wheel[] _wheels;
        private Body _body;

        public Car()
        {
            _wheels = new Wheel[2];
            _sprite = new Sprite();
            _body = new Body();

            _sprite.addChild(_body.disp);
//            _body.disp.showBoundary = true;

            for (int i = 0; i < _wheels.Length; i++ )
            {
                _wheels[i] = new Wheel();
                _wheels[i].disp.y = 25;
                
                //_wheels[i].disp.showBoundary = true;
                //_wheels[i].disp.showFrame = true;
                
                _sprite.addChild(_wheels[i].disp);
            }            
            
            _wheels[0].disp.x = 8;
            _wheels[1].disp.x = 46;

            _wheels[0].disp.pivot = new Point(15, 15);
            _wheels[1].disp.pivot = new Point(15, 15);
        }

        public void OnUpdate(float deltaTime)
        {
            float wheelFactor = 300;            
            deltaTime /= 1000.0f;
            wheelFactor *= deltaTime;            
            for (int i = 0; i < _wheels.Length; i++)
            {
                _wheels[i].disp.rotateZ += wheelFactor;                
            }
        }

        public Sprite disp
        {
            get
            {
                return _sprite;
            }
        }
    }
}
