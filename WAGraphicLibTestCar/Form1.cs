﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NGraphics;
using WA.GraphicLib;

namespace WAGraphicLibTestCar
{
    public partial class Form1 : Form
    {
        private Stage _stage;        
        private Car _car;

        public Form1()
        {
            InitializeComponent();
            _stage = new Stage(graphicControl1);
            _stage.color = 0xFFFFFF;
            _stage.renderUpdateTime = 1000 / 30;
            this.Load += _OnLoad;
        }

        private void _OnLoad(object sender, EventArgs e)
        {
            /*
            Quad rect = new Quad();
            rect.brush = new LinearGradientBrush(Point.Zero, Point.OneY, Colors.Blue, Colors.White);
            _stage.add(rect);
            rect.draw(0, 0, _stage.width, _stage.height);*/          
            _car = new Car();

            _car.disp.x =
            _car.disp.y = 100;

            _car.disp.scaleX = 
            _car.disp.scaleY = 1.5f;
  //        _car.disp.showBoundary = true;

            _stage.add(_car.disp);            
            _stage.addEventListener(EventType.StageOnUpdate, (evt) =>
            {
                textBox1.Text = "(" + Stage.CursorPos.X + "," + Stage.CursorPos.Y + ")";
                float deltaTime = (float)evt.data;
                float carFactor = 100;
                _car.OnUpdate(deltaTime);
                deltaTime /= 1000.0f;
                carFactor *= deltaTime;

//              _car.disp.scaleX += carFactor/50;
//              _car.disp.scaleY += carFactor/50;
                             
                _car.disp.x += carFactor;
                if (_car.disp.x > _stage.width)
                {
                    _car.disp.x = 0;
                }
            });
        }
    }
}
