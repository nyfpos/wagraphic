﻿using NGraphics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WA.GraphicLib;

namespace WAGraphicLibTest
{
    public partial class Form1 : Form
    {
        private Stage _stage;

        private void _Loginfo(string format, params object[] args)
        {
            textBox1.Text = String.Format(format, args);
        }

        public Form1()
        {            
            InitializeComponent();            
            _stage = new Stage(graphicControl1);
            _stage.color = 0x333333;
            _stage.addEventListener(EventType.StageOnUpdate, (evt) => textBox2.Text = "(" + Stage.CursorPos.X + "," + Stage.CursorPos.Y + ")" );

            WA.GraphicLib.Ellipse ell = new WA.GraphicLib.Ellipse();
            ell.color = 0xFF0000;
            ell.beFillColor = true;
            ell.fillColor = 0xFFFF00;
            ell.border = 2.0f;                        
            Quad rect = new Quad();
            rect.brush = new LinearGradientBrush(Point.Zero, Point.OneY, Colors.Blue, Colors.White);
            _stage.add(rect);
            _stage.add(ell);

            Line line = new Line();
            _stage.add(line);

            line.color = 0;
            line.draw(new Point(0, 0), new Point(_stage.width, _stage.height));
            line.visible = false;

            ell.visible = false;
            ell.draw(10, 10, 100, 100);
            rect.draw(0, 0, _stage.width, _stage.height);

            Sprite sprite = new Sprite();

            Quad[] rects = new Quad[]
            {
                new Quad(),
                new Quad(),
                new Quad(),
                new Quad()
            };
            uint[] colors = new uint[]
            {
                0xF4A460,
                0x8B4513,
                0x20B2AA,
                0x7FFFD4
            };

            
            Rect[] drawinfos = new Rect[]
            {
                new Rect(10,10,50,30),
                new Rect(35,35,50,30),
                new Rect(60,60,50,30),
                new Rect(85,85,50,30),
            };
            
            /*
            Rect[] drawinfos = new Rect[]
            {
                new Rect(0,0,50,30),
                new Rect(25,25,50,30),
                new Rect(50,50,50,30),
                new Rect(75,75,50,30),
            };
            */
            /*
            Rect[] drawinfos = new Rect[]
            {
                new Rect(100,100,50,30),
                new Rect(125,125,50,30),
                new Rect(150,150,50,30),
                new Rect(175,175,50,30),
            };
            */
            /*
            var rec = new Quad();
            rec.beFillColor = true;
            rec.color = rec.fillColor = 0xF4A460;
            rec.alpha = rec.fillAlpha = 0.5f;
            rec.draw(10, 10, 50, 30);
            sprite.addChild(rec);*/
            //rec.x = rec.y = 240;
            //_stage.add(rec);

            for (int i = 0; i < rects.Length; i++ )
            {
           //     rects[i].x = rects[i].y = 10;      
                rects[i].name = "rec_" + i;
                rects[i].beFillColor = true;
                rects[i].color = 
                rects[i].fillColor = colors[i];
                rects[i].alpha =
                rects[i].fillAlpha = 0.5f;                
                rects[i].draw((float)drawinfos[i].X, (float)drawinfos[i].Y, (float)drawinfos[i].Width, (float)drawinfos[i].Height);
                sprite.addChild(rects[i]);
            }

            sprite.name = "sprite";
            sprite.showBoundary = true;
            sprite.showFrame = true;
            sprite.x = sprite.y = 240;


            bool startR = false;
            _stage.add(sprite);
                //   textBox1.Text += "ell.boundary(" + ell.x + "," + ell.y + "," + ell.width + "," + ell.height + ")\r\n";
                //   textBox1.Text += "rect.boundary(" + rect.x + "," + rect.y + "," + rect.width + "," + rect.height + ")\r\n";            
            Logger.Instance.BindLogInfo(_Loginfo);
            float _f = 6.0f;
            float during = 0.0f;
                _stage.addEventListener(EventType.StageOnUpdate, (evt) =>
                {
                    float factor = 20, factor2 = 0.15f;
                    float deltaTime = (float)evt.data;
                    deltaTime /= 1000.0f;
                    factor *= deltaTime;
                    factor2 *= deltaTime;
       //             ell.x += factor;
      //              ell.y += factor;
                   /*
                    foreach (DisplayObject disp in sprite.each())
                    {
                        disp.rotateZ += (factor*4);
                        disp.rotateZ %= 360;
                    }
                    */
//                    if (startR  || (sprite.x >= (_stage.width /3)))
                    {
                     //   rec.rotateZ += factor;                  
                        sprite.rotateZ += factor;                  
                        startR = true;
               //         _Loginfo("{0},{1}", sprite.x, sprite.y);
/*
                        during += deltaTime;
                        if (during < _f)
                        {
                            sprite.scaleX += factor2;
                            sprite.scaleY += factor2;

                            if (sprite.scaleX >1.5f)
                                sprite.scaleX = 1.5f;
                            if (sprite.scaleY > 1.5f)
                                sprite.scaleY = 1.5f;
                        }
                        else if (during >= _f && during < 2*_f)
                        {
                            sprite.scaleX -= factor2*1.1f;
                            sprite.scaleY -= factor2 * 1.1f;
                            if (sprite.scaleX < 0.1f)
                                sprite.scaleX = 0.1f;
                            if (sprite.scaleY < 0.1f)
                                sprite.scaleY = 0.1f;
                        }
                        else
                        {
                            during = 0;
                        }
                    */}
                   /* else
                    {
                        sprite.x += factor;
                        sprite.y += factor;
                    }*/
//                  ell.width += factor;
//                  ell.height += factor;                   
                    //textBox1.Text = deltaTime.ToString()+"\r\n";
                    //textBox1.Text = "ell.boundary(" + ell.x + "," + ell.y + "," + ell.width + "," + ell.height + ")\r\n";
                });
     
            /*
            Timer t = new Timer();
            t.Interval = 1000;
            t.Tick += (s, e) =>
            {
//                t.Stop();
                ell.width += 1;
                ell.height += 1;
            };
            t.Start();    */        
        }
    }
}
