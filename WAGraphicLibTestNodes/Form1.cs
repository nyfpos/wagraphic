﻿//#define LIB_TEST1
#define LIB_TEST2

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WA.GraphicLib;
using WA.Node;
using NGraphics;

namespace WAGraphicLibTestNodes
{
    public partial class Form1 : Form
    {
#if ShowCurveTestRange
        static public Stage getStage
        {
            get
            {
                return _stage;
            }
        }
        static 
#endif
        private Stage _stage;
        private WA.Node.View _view;
        static private float _ScaleSize = 0.6f;

        private void _Loginfo(string format, params object[] args)
        {
            textBox2.Text = String.Format(format, args);
        }
        private void _LoginfoAppend(string format, params object[] args)
        {
            textBox2.Text += String.Format(format, args);
            textBox2.Text += "\r\n";
            textBox2.SelectionStart = textBox2.TextLength;
            textBox2.ScrollToCaret();
        }

        public Form1()
        {
            InitializeComponent();            
            _stage = new Stage(graphicControl1);
            _stage.color = 0xFFFFFF;            
            _stage.addEventListener(EventType.StageOnUpdate, (evt) => textBox1.Text = "(" + Stage.CursorPos.X + "," + Stage.CursorPos.Y + ")");
#if LIB_TEST1
            _stage.addEventListener(EventType.StageOnLoaded, _OnLoaded);
#endif
#if LIB_TEST2
            Logger.Instance.BindLogInfo(_LoginfoAppend);
            Logger.Instance.BindClearLogInfo(_ClearLogInfo);
            _view = new WA.Node.View(_stage);            
            _stage.addEventListener(EventType.StageOnLoaded, _OnLoaded2);
            _stage.addEventListener(EventType.StageOnKeyDown, _OnStageKeyDown);
#endif
        }
#if LIB_TEST2
        private void _OnLoaded2(Event evt)
        {
            Logger.Instance.BindLogInfo(_LoginfoAppend);
            _view.start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Image img = Image.FromFile(@"../../../Resource/project-S.png");
            Node node = new Node(img, NodeType.StartNode);
            node.disp.scaleX =
            node.disp.scaleY = _ScaleSize;
            _view.add(node);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Image img = Image.FromFile(@"../../../Resource/Scada-S.png");
            Node node = new Node(img, NodeType.Both);
            node.disp.scaleX =
            node.disp.scaleY = _ScaleSize;
            _view.add(node);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Image img = Image.FromFile(@"../../../Resource/serial-S.png");
            Node node = new Node(img, NodeType.Both);
            node.disp.scaleX =
            node.disp.scaleY = _ScaleSize;
            _view.add(node);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Image img = Image.FromFile(@"../../../Resource/ADAM4000-S.png");
            Node node = new Node(img, NodeType.EndNode);
            node.disp.scaleX =
            node.disp.scaleY = _ScaleSize;
            _view.add(node);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
        }

        private void _ClearLogInfo() { textBox2.Text = ""; }

        private void button6_Click(object sender, EventArgs e)
        {
            _deleteFocusItem();
        }

        private void _OnStageKeyDown(Event e)
        {
            KeyEventArgs arg = (KeyEventArgs)e.data;
            if (arg.KeyCode == Keys.Delete) // delete
            {
                _deleteFocusItem();
            }
        }

        private void _deleteFocusItem()
        {
            Node[] focusNds = null;
            Wire focusWire = null;
            _view.getFocusItem(out focusNds, out focusWire);
            _view.deleteItem(focusNds, focusWire);
        }

#endif
#if LIB_TEST1
        private void _OnLoaded(Event evt)
        {
            Logger.Instance.BindLogInfo(_LoginfoAppend);
           
            DrawPath path = new DrawPath();
            _stage.add(path);

            path.border = 3.0f;
            path.color = 0x808080;

            Image img = Image.FromFile(@"../../../Resource/ADAM4000-S.png");
            Node node = new Node(img);
            _stage.add(node.disp);
            
            node.disp.scaleX =
            node.disp.scaleY = 0.7f;
            node.disp.interactive.bedrag = true;
            

            bool beStartLink = false;
            NGraphics.Point linkStartPt = NGraphics.Point.Zero;

            node.disp.addEventListener(NodeEventType.OnJoinMouseDown, (e) =>
            {
                EventJoinArg arg = (EventJoinArg)e.data;
                linkStartPt = arg.pos;
                _Loginfo("{0},{1}", linkStartPt.X, linkStartPt.Y);
                node.disp.interactive.bedrag = false;
                beStartLink = true;
            });

            node.disp.addEventListener(NodeEventType.OnJoinMouseUp, (e) =>
            {

                beStartLink = false;
            });

            float nw = (int)node.disp.width,
                  nh = 3;
            _stage.addEventListener(EventType.StageOnUpdate, (e) => 
            { 
                    if (beStartLink)
                    {
                        NGraphics.Point st_pt = linkStartPt;
                        NGraphics.Point ed_pt = Stage.CursorPos;

                        float dx = (float)(ed_pt.X - st_pt.X);
                        float dy = (float)(ed_pt.Y - st_pt.Y);
                        float delta = (float)Math.Sqrt(dy * dy + dx * dx);
                        float scale = 1;
                        if (delta < nw)
                        {
                            scale = 0.75f - 0.75f * ((nw - delta) / nw);
                        }
                        path.draw(new List<PathOp>()
                        {
                            new MoveTo(st_pt),
                            new CurveTo(new NGraphics.Point(st_pt.X+scale*nw, st_pt.Y+nh),
                                        new NGraphics.Point(ed_pt.X-scale*nw, ed_pt.Y-nh),
                                        ed_pt)
                        });
                    }
                    else
                    {
                        path.erase();
                        node.disp.interactive.bedrag = true;
                    }
            });
        }
#endif
    }
}
